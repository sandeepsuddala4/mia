/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');

module.exports = function(app) {

  // Insert routes below
  // route to generate jwt token using info to authenticate for next requests
  app.use('/api/auth', require('./api/auth'));
  app.use('/api/feedback', require('./api/feedback'));
  app.use('/api/logs', require('./api/logs'));
  app.use('/api/admin', require('./api/admin'));
  app.use('/api/userSpellingCorrection', require('./api/spellchecker'))

  app.use('/app', require('../client/chat'));

  app.use('/admin/dashboard', require('../client/admin'))
  
  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|assets)/*')
   .get(errors[404]).get(errors[500]);

  // All other routes should redirect to the index.html
  console.log(app.get('appPath'));
  app.route('/*')
    .get(function(req, res) {
     // res.sendFile(app.get('appPath')  + '/index.html');
      res.render('index')
    });

    app.use(function(err, req, res, next) {
      // Do logging and user-friendly error message display
      console.error(err);
      // Assuming that template engine is plugged in
      res.render('500');
    })
};
