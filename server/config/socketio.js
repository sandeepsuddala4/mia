/**
 * Socket.io configuration
 */

'use strict';

var config = require('./environment');
const jwt = require('jsonwebtoken');
const dialogFlow = require('../components/dialogbox/dialogflow.controller');
const encryptor = require('../components/util/crypto');
const jwtDB = require('../api/auth/jwt.db');
const url = require('url');
const verifyOptions = {
    "alg": "HS256",
    "typ": "JWT"
}

// When the user disconnects.. perform this
function onDisconnect(socket) {
  socket.counterQuestion = 1;
}

// When the user connects.. perform this
function onConnect(socket) {
  // When the client emits 'info', this listens and executes
  socket.on('info', function (data) {
    logger.info('[%s] %s', socket.address, JSON.stringify(data, null, 2));
  });

  // Insert sockets below
  //require('../api/thing/thing.socket').register(socket);
}

function onMessageRecieved(socket, dialogFlowApp,message) {
  socket.emit('chat message', 'Me: ' + message);
  dialogFlow.getDialogFlowResponse(dialogFlowApp,socket,message);
}


module.exports = function (socketio, dialogFlowApp) {
  // socket.io (v1.x.x) is powered by debug.
  // In order to see all the debug output, set DEBUG (in server/config/local.env.js) to including the desired scope.
  //
  // ex: DEBUG: "http*,socket.io:socket"

  // We can authenticate socket.io users and access their token through socket.handshake.decoded_token
 
  socketio.use(require('socketio-jwt').authorize({
    secret: config.requiredProcessEnv('JWT_SECRET'),
    handshake: true
  }));

  socketio.on('connection', function (socket) {
    socket.use((packet, next) => {
      logger.info('[%s] CONNECTED', socket.address);
    socket.address = socket.handshake.address !== null ?
            socket.request.connection.remoteAddress + ':' + socket.request.connection.remotePort :
            process.env.DOMAIN;
    if (socket.handshake.query && socket.handshake.query.token){
      var token = socket.handshake.query.token;
      jwt.verify(token, config.requiredProcessEnv('JWT_SECRET'), verifyOptions,function(err, decoded) {
        if(err){ 
          next(new Error('Authentication error'));
        }
        else {
          socket.decoded = decoded;
          var userDetails = encryptor.decryptUserDetails(decoded.apd);
          var referer = socket.handshake.headers.referer;
          
          logger.log(userDetails);
          var jwtuser = new Object({
            associateId: userDetails.associateId,
            token: token
          })
          jwtDB.isValidToken(jwtuser, function(isValid){
            logger.log("isValid ----- inside chat --- "+isValid);
            if(isValid){
              socket.decoded = userDetails;
              if(referer){
                var u = url.parse(referer);
                logger.log(u);
                if(!u ||  config.allowedReferers.indexOf(u.host) === -1) {
                  next(new Error('Authentication error'));
                }
                if(userDetails.refUrl.indexOf(u.host) === -1){
                  next(new Error('Authentication error'));
                }
            
                socket.decoded = userDetails;
                next();
              }
              else {
                 next(new Error('Authentication error'));
              }
            }
            else {
              next(new Error('Authentication error'));
            }
          })
          
        }
      });
    } else {
      next(new Error('Authentication error'));
    }    
    });
    socket.connectedAt = new Date();

    // Call onDisconnect.
    socket.on('disconnect', function () {
      onDisconnect(socket);
      logger.info('[%s] DISCONNECTED', socket.address);
    });

    socket.on('chat message', function (message) {
      logger.info('[%s] MESSAGED', socket.address);
      onMessageRecieved(socket, dialogFlowApp,message);
      //socket.emit('chat message', 'Me: ' + msg);
    });


    logger.info('[%s] CONNECTED', socket);
    // Call onConnect.
    onConnect(socket);
    logger.info('[%s] CONNECTED', socket.address);

  });
};