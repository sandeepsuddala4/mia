'use strict';

// Production specific configuration
// =================================
module.exports = {
  // Server IP
  ip:       process.env.IP ||
            'localhost',
  // Server port
  port:     process.env.PORT ||
            3000,
  // MongoDB connection options
  mongo: {
    uri:    process.env.MONGODB_URL,
    options: {
      db: {
        safe: true,
      },
      useMongoClient:true,
      useNewUrlParser: true
    }
  },
  SEND_EMAIL: true,
  JWT_GENERATOR_PARAMS: {
    iss:'client-prod',
    aud:'globalview-prod',
    tenant:'540'
  },
  API_HEADERS :{
    "Username": "testuser", 
    "Password": "test123"
  },
  PAY_JWT_PARAMS: {
    iss:'ADP',
    aud:'globalview-prod',
    tenant:'540',
  },
  GV_URL: 'portal101.globalview.adp.com'
};