'use strict';

// Test specific configuration
// ===========================
//'mongodb://root:KVcnSeTwEqE2@localhost'
module.exports = {
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://root:KVcnSeTwEqE2@localhost' ,
    options: {
      db: {
        safe: true
      },
      useNewUrlParser: true
    }
  },
  API_HEADERS :{
    "Username": "testuser", 
    "Password": "test123"
  },
  JWT_GENERATOR_PARAMS: {
    iss:'client-uat',
    aud:'globalview-uat',
    tenant:'540'
  },
  PAY_JWT_PARAMS: {
    iss:'ADP',
    aud:'globalview-uat',
    tenant:'540',
  },
  proxy: {
    url: 'hydproxy.es.oneadp.com',
    port: 8080
  },
  SEND_EMAIL:true,
  GV_URL: 'portal102.globalview.adp.com'
};