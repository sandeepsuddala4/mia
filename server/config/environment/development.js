'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/mia',
    options: {
      db: {
        safe: true
      },
      useNewUrlParser: true
    }
  },
  cors:[
    'http://localhost:3000'
  ],
  proxy: {
    url: 'hydproxy.es.oneadp.com',
    port: 8080
  },
  SEND_EMAIL:true,
  API_HEADERS :{
    "Username": "testuser", 
    "Password": "test123"
  },
  JWT_GENERATOR_PARAMS: {
    iss:'client-rddev',
    aud:'globalview-rddev',
    tenant:'002'
  },
  PAY_JWT_PARAMS: {
    iss:'LT1DEV',
    aud:'globalview-rddev',
    tenant:'002',
  },
  GV_URL: 'portal106.globalview.adp.com',
};
