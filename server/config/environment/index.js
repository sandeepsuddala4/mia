'use strict';

var path = require('path');
var _ = require('lodash');

function requiredProcessEnv(name) {
  if(!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable');
  }
  return process.env[name];
}

// All configurations will extend these options
// ============================================
var all = {
  env: process.env.NODE_ENV,

  // Root path of server
  root: path.normalize(__dirname + '/../../..'),

  // Server port
  port: process.env.SERVER_PORT || 3000,

  // MongoDB connection options
  mongo: {
    options: {
      db: {
        safe: true
      },
      useNewUrlParser: true
    }
  },
  requiredProcessEnv: requiredProcessEnv,
  accessControlAllowOrigin : "http://hyrdmoss01:8080",
  defaultFallBackIntentName : "Default Fallback Intent",
  defaultIntentFallBackQuestion : "I'm still learning, please elaborate the question",
  defaultIntentFallBackQuestionUnableToAnswer : "Sorry am learning and unable to understand your question ☹️ ...Please check with HR for information",
  counterQuestionLimit : 2,
  supportEmails : ['Sundeep.Suddala@adp.com'],
  itSupportEmails :['Sundeep.Suddala@adp.com'],
  allowedReferers : ['http://hyrdmoss01:8080', 'http://localhost:3000', 'localhost:3000', 'http://hyrdmoss01:8080/','http:/hyrdmoss01:8080/' ]
  
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
  all,
  require('./' + process.env.NODE_ENV + '.js') || {});