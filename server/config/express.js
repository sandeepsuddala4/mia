/**
 * Express configuration
 */

'use strict';

var express = require('express');
var morgan = require('morgan');
var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var errorHandler = require('errorhandler');
var path = require('path');
var config = require('./environment');
const frameguard = require('frameguard')

morgan('combined');
// a format string
morgan(':remote-addr :method :url :uuid');
// a custom function
morgan(function (req, res) {
  return req.method + ' ' + req.url + ' ' + req.uuid;
})
const logger = require('../components/util/logger');
global.logger = logger;

function ignoreFavicon(req, res, next) {
  if (req.originalUrl === '/favicon.ico') {
    res.status(204).json({nope: true});
  } else {
    next();
  }
}

module.exports = function(app) {
  var env = app.get('env');

  app.set('views', config.root + '/client/views');
  app.set('view engine', 'ejs');
  app.use(compression());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(methodOverride());
  app.use(cookieParser());
  app.use(ignoreFavicon);

  app.all('*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', config.accessControlAllowOrigin);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Credentials', false);
    res.header('Access-Control-Max-Age', '86400');
    res.header('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers,newheader,X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    next();
  });

  if ('production' === env) {
    //app.use(express.static(path.join(config.root, 'public')));
    //app.set('appPath', config.root + '/public');
    app.use(frameguard({
      action: 'allow-from',
      domain: config.accessControlAllowOrigin
    }))
    app.use(express.static(path.join(config.root, '.tmp')));
    app.use(express.static(path.join(config.root, 'client')));
    app.use(morgan(''));
    app.use(errorHandler());
  }

  if ('development' === env || 'test' === env) {
    app.use(express.static(path.join(config.root, '.tmp')));
    app.use(express.static(path.join(config.root, 'client')));
    app.use(express.static(path.join(config.root, 'client/admin')));
    app.set('appPath', 'client');
    app.use(morgan('dev'));
    app.use(errorHandler()); // Error handler - has to be last
  }
};