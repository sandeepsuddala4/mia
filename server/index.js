/**
 * Main application file
 */

'use strict';

require('dotenv').config();

var express = require('express');
var mongoose = require('mongoose');

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var config = require('./config/environment');

// Connect to database
mongoose.connect(config.mongo.uri);


// Setup server
var app = express();
var server = require('http').createServer(app);
var socketio = require('socket.io')(server);
var dialogFlowApp = require('./components/dialogbox');
require('./config/socketio')(socketio, dialogFlowApp);
require('./config/express')(app);
require('./routes')(app);

var logger = require('./components/util/logger');

process.on('uncaughtException', (err) => {
  logger.error("Un caught Exception ", err);
  logger.log(`Caught exception: ${err}\n`);
});

// Start server
server.listen(config.port, config.ip, function () {
  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});

// Expose app
exports = module.exports = app;