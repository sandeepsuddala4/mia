/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-04 10:25:21 
 * @Last Modified by:   Sundeep Suddala 
 * @Last Modified time: 2019-03-04 10:25:21 
 */

'use strict';

module.exports = {
    ASSOCIATE_DETAILS : '/mnccc/v1/mncapi/associateDetailsUsingLoginName?loginName={{0}}&grant_type=password',
    LEAVE_BALANCES : '/mnccc/v1/mncapi/leaveUserBalances?grant_type=password&id={{0}}',
    ASSOCIATE_HEIRARCHY_DETAILS : '/mnccc/v1/mncapi/associateReportingHeirarchy?grant_type=password&id={{0}}', 
    PERSONAL_IDENTIFIERS : '/mnccc/v1/mncapi/personalIdentifiers?empId={{empId}}&code={{codeName}}&country=IN',
    MEDICLAIM_DETAILS : '/mnccc/v1/mncapi/mediclaimDetails?grant_type=password&id={{0}}',
    GTL_NOMINEES : '/mnccc/v1/mncapi/gtlNomineeDetails?grant_type=password&id={{0}}',
    FPR_RATINGS : '/mnccc/v1/mncapi/associateAppraisalDetails?grant_type=password&id={{0}}',
    GLB_DETAILS : '/mnccc/v1/mncapi/associateGLBDetails?grant_type=password&id={{0}}',
    ASSOCIATE_CLAIMS : '/mnccc/v1/mncapi/claims?empId={{0}}&code={{code}}&country={{country}}',
    ASSOCIATE_PERSONAL_DETAILS : '/mnccc/v1/mncapi/personalIdentifiers?empId={{0}}&code={{code}}&country={{country}}',
    PAY_DETAILS_URLS : {
        GET_PORTAL_KEY : '/mnccc/v1/mncapi/portalUserId?empId={{0}}',
        PAY_STATEMENTS : '/mnccc/gvapi/v1/payStatements',
        PAY_STATEMENT : '/mnccc/gvapi/v1/payStatement/{{0}}'
    },
    PTD_DEDUCTIONS_URL : '/mnccc/v1/mncapi/ptdDeductions?empId={{0}}&code={{codeName}}&country={{country}}&startDate=00000000&endDate={{endDate}}',
    PTD_DEDUCTIONS_CODES :{
        YCC: '7407',
        NPS: '7620',
        VPF: '/3F2'
    }
}