const https = require('https');
const jwtProvider = require('../util/jwt-provider');
const constants = require('../constants');
const jwt =  require('jsonwebtoken');
const env = require('../../config/environment');
const url = require('url');
var util = require('../../components/util');
var dateUtil = require('../../components/util/dateUtil');
var encryptor = require('../util/crypto');
var jwtUserDB = require('../../api/auth/jwt.db');

module.exports.getGVAPIResponse = function (options, callback) {
    var token = jwtProvider.generateJWT(options.sub);
    var headers = env.API_HEADERS;
    headers['Authorization'] = token;
    var reqOptions = {
        host: env.GV_URL,
        path: options.path,
        headers: headers
    }
    var req = https.get(reqOptions, function (res) {
        res.setEncoding('utf8');
        var data = "";
        res.on("data", function (chunk) {
            data += chunk;
        });

        res.on('error', function (err) {
            logger.error("Error while request", reqOptions);
            logger.error("",err);
        });

        res.on("end", function () {
            logger.log("On GV response End");
            logger.log(data);
            callback(data);
        });
    })
}

module.exports.initChat = function (request, options, response) {
    var token = jwtProvider.generateJWT(options.sub);
    var headers = env.API_HEADERS;
    
    headers['Authorization'] = token;
    var reqOptions = {
        host: env.GV_URL,
        path: options.path,
        headers: headers
    }
    //logger.log(reqOptions)
    var req = https.get(reqOptions, function (res) {
        res.setEncoding('utf8');
        var data = "";
        res.on("data", function (chunk) {
            data += chunk;
        });

        res.on('error', function (err) {
            logger.error("Error while request", reqOptions);
            logger.error("",err)
        });

        res.on("end", function () {
            logger.log(data)
            var all_data = JSON.parse(data);
            var referer = request.headers.referer || '';
            var refUrl = util.getHostUrl(referer);
            var newData = {};
            if(all_data != undefined && all_data.data != undefined && all_data.data.associate != undefined && all_data.data.associate.associateId != undefined){
                var associateDetails = all_data.data.associate;
                newData.associateName = associateDetails.firstName+','+associateDetails.lastName;
                newData.associateId = associateDetails.associateId;
                newData.loginName = associateDetails.loginName;
                newData.url = request.protocol + "://" + request.get('host')
                try {
                    if(associateDetails.dateOfBirth){
                        var date =associateDetails.dateOfBirth.split("-");
                        var dateOfBirth =  new Date(date[2], date[1] - 1, date[0]);
                        var currentDate = new Date();
                        if(dateOfBirth && currentDate.getMonth() === dateOfBirth.getMonth() && currentDate.getDate() === dateOfBirth.getDate()){
                            newData.apdd= "t";
                        }
                    }

                    if(associateDetails.weddingDate){
                        var date =associateDetails.weddingDate.split("-");
                        var weddingDate =  new Date(date[2], date[1] - 1, date[0]);
                        var currentDate = new Date();
                        if(weddingDate && currentDate.getMonth() === weddingDate.getMonth() && currentDate.getDate() === weddingDate.getDate()){
                            newData.apdw= "t";
                        }
                    }
                }
                catch(e){
                    logger.error(e);
                }
                if(refUrl != '')
                    newData.refUrl = refUrl;

                var encryptedData = encryptor.encryptUserDetails(newData);
                var jwtData = new Object({
                    apd:encryptedData,
                    iss:newData.url
                });
                // var options = new Object({
                //     expiresIn: env.requiredProcessEnv('JWT_TIME_OUT')
                // })
                var token = jwt.sign(jwtData, env.requiredProcessEnv('JWT_SECRET'));
                var jwtUser = jwtUserDB.newUser();
                jwtUser.associateId = associateDetails.associateId;
                jwtUser.token =  token;
                jwtUser.createdDate = new Date();            
                jwtUserDB.saveToken(jwtUser);
                var responseData = {};responseData['token'] = token;         
                response.send(responseData);
            }else {
                if(all_data != undefined && all_data.data != undefined && all_data.data.associate && Object.keys(all_data.data.associate).length ==0){
                    response.status(401).send({error: "Invalid user name"});
                }
                else {
                    response.status(500).send({error: "Error while fetching user details"});
                }
            }
        });
    });

    
}

module.exports.getPayDetails = function(options, callback) {
    //if(process.env.NODE_ENV === 'production'){ 
        options['sub'] = options.id;
        options['path'] = constants.PAY_DETAILS_URLS.GET_PORTAL_KEY.replace('{{0}}', options.sub);
    //}
    //else { // for test and development purpose dummy data
      //  options['path'] = constants.PAY_DETAILS_URLS.GET_PORTAL_KEY.replace('{{0}}','414904');
       // options['sub'] = '414904';
   // }
    var token = jwtProvider.generateJWT(options.sub);
    var headers = env.API_HEADERS;
    headers['Authorization'] = token;
    var reqOptions = {
        host: env.GV_URL,
        path: options.path,
        headers: headers
    }
    var req = https.get(reqOptions, function (res) {
        res.setEncoding('utf8');
        var data = "";
        res.on("data", function (chunk) {
            data += chunk;
        });

        res.on('error', function (err) {
            logger.error("Error while request", reqOptions);
            logger.error(err);
            //response.send(err);
        });

        res.on("end", function () {
            logger.log("On GV response End")
            logger.log(data);
            data = JSON.parse(data);
            if(data != undefined  && data.data != undefined && data.data.portalUserId ){
                var payDetails = getPayStatements(data.data.portalUserId, function(payStatement){
                    callback(payStatement);
                });
            }
            else {
                callback(data)
            }
        });
    });
}


function getPayStatements(portalUserId, callback) {
    logger.log("--------------------------------------getPayStatements-------------------------------");
    var token = jwtProvider.generateJWT(portalUserId, true);
    var payDetailsUrl = constants.PAY_DETAILS_URLS.PAY_STATEMENTS;
    var reqOptions = {
        host: env.GV_URL,
        path: payDetailsUrl,
        headers: {
            "Authorization": token
        }
    }
    var payDetailsReq = https.get(reqOptions, function (res) {
        res.setEncoding('utf8');
        var data = "";
        res.on("data", function (chunk) {
            data += chunk;
        });

        res.on('error', function (err) {
            logger.error("Error while paydetails request");
            logger.error("",err);
        });

        res.on("end", function () {
            logger.log("On PAY DETAILS response End")
            logger.log(data)
            //getPayStatements(data);
            if(data != undefined){
                var parsedData ;
                try {
                    parsedData = JSON.parse(data);
                    if(parsedData.payStatements != undefined && parsedData.payStatements.length > 0){
                        var payStatements = parsedData.payStatements;
                        var latestStatement ;
                        if(payStatements.length > 1){
                            payStatements.sort(function (a, b) {
                                var aDate = a.payDate.split("-")
                                aDate =  new Date(aDate[2], aDate[1], aDate[0]);
                                var bDate = b.payDate.split("-")
                                bDate =  new Date(bDate[2], bDate[1], bDate[0]);
                                return dateUtil.dates.compare(aDate,bDate);
                            });
                            latestStatement = payStatements[0];
                        }
                        else {
                            latestStatement = payStatements[0];
                        }
                        var statementId = latestStatement.payDetailUri.href;
                        statementId = statementId.split('payStatement/')[1];
                        //logger.log(statementId)
                        getPayStatementOfSpecificMonth(portalUserId,statementId, function(payStatement){
                            callback(payStatement);
                        });
                    }
                }
                catch( e ){
                    logger.error("Error while request", reqOptions);
                    logger.error("",e)
                    return parsedData;
                }
            }
        });
})
}



function getPayStatementOfSpecificMonth( portalUserId, statementId, callback ){
    logger.log("--------------------------------------getPayStatementsSpecificMonth-------------------------------");
    var token = jwtProvider.generateJWT(portalUserId, true);
    var payDetailsUrl = constants.PAY_DETAILS_URLS.PAY_STATEMENT.replace('{{0}}', statementId);
    var reqOptions = {
        host: env.GV_URL,
        path: payDetailsUrl,
        headers: {
            "Authorization": token
        }
    }
    var payDetailsReq = https.get(reqOptions, function (res) {
        res.setEncoding('utf8');
        var data = "";
        res.on("data", function (chunk) {
            data += chunk;
        });

        res.on('error', function (err) {
            logger.error("Error while request", reqOptions);
            logger.error("Error Payment Services",+err);

        });

        res.on("end", function () {
            logger.log("OngetPayStatementOfSpecificMonth response End")
            callback(data);
        });
    });
}
