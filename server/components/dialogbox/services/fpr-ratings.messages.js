/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-13 11:29:13 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-03-13 17:12:50
 */


 
'use strict'

/**
 * Rest Client Module to make calls to GV 
 */
const restClient = require('../../../components/rest');
/**
 * Constants which contain GV urls
 */
const constants = require('../../../components/constants');

/**
 * Method to get FPR rating of an associate
 * @param {*} requestParams Request Parameters to be sent with the request
 * @param {*} responseAPIAI Response from DialogFlow, to use parameters and intents from it
 * @param {*} callback Callback method to send response
 */

module.exports.getAssociateFPRRatings = function(requestParams, responseAPIAI, callback) {
    requestParams['path'] = constants.FPR_RATINGS.replace('{{0}}', requestParams.id);
    restClient.getGVAPIResponse(requestParams, function(body){
        logger.debug(body);
        var response = JSON.parse(body);
        var ratingResponse = {};
        ratingResponse.messageType = 'fprRatings'
        try {
            ratingResponse.appraisalList = response.data.associate.appraisalList;
        }
        catch(e){
            ratingResponse.appraisalList = [];
        }
        callback(ratingResponse);
    })
    
}