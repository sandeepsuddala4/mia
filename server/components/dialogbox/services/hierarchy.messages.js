'use strict';


/**
 * Rest Client Module to make calls to GV 
 */
const restClient = require('../../../components/rest');
/**
 * Constants which contain GV urls
 */
const constants = require('../../../components/constants');

/**
 * Method to return associate hierarchy of an associate
 * @param {*} requestParams Request Parameters to be sent with the request
 * @param {*} responseAPIAI Response from DialogFlow, to use parameters and intents from it
 * @param {*} callback Callback method to send response
 */
module.exports.getAssociateHeirarchyDetails = function(requestParams, responseAPIAI, callback) {
    requestParams['path'] = constants.ASSOCIATE_HEIRARCHY_DETAILS.replace('{{0}}', requestParams.id);
    //logger.debug("inside getAssociateHeirarchyDetails")
    restClient.getGVAPIResponse(requestParams, function(body){
        logger.log('inside getAssociateHeirarchyDetails')
        logger.log(body)
        logger.debug(body);
        body = JSON.parse(body);
        var reportingHeirarchy = body.data.associate.reportingHeirarchy;
        var index = Object.keys(reportingHeirarchy).length;
            var message ='';
            message = 'Take a look at your hierarchy';
            if(reportingHeirarchy){
                var margin = 0;
                var list =''
                while(index >= 1){
                    logger.log(reportingHeirarchy[index+""])
                    var data = reportingHeirarchy[index+""];
                    var mm = '<li style="list-style-type:disc;margin-left:'+margin+'px">'+data.firstName +' '+data.lastName+'</li>'
                    list+= mm;
                    margin = margin+5;
                    index = index -1;
                }
                message += '<ul>'+list+'</ul>';
                callback(message);
            }
    })
}