/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-13 11:45:45 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-03-13 13:41:52
 */


 
'use strict'


/**
 * Rest Client Module to make calls to GV 
 */
const restClient = require('../../../components/rest');
/**
 * Constants which contain GV urls
 */
const constants = require('../../../components/constants');

/**
 * Method to get GLB dues of an associate
 * @param {*} requestParams Request Parameters to be sent with the request
 * @param {*} responseAPIAI Response from DialogFlow, to use parameters and intents from it
 * @param {*} callback Callback method to send response
 */
module.exports.getAssociateGLBDues = function(requestParams, responseAPIAI, callback) {
    requestParams['path'] = constants.GLB_DETAILS.replace('{{0}}', requestParams.id);
    restClient.getGVAPIResponse(requestParams, function(body){
        logger.debug(body);
        var response = JSON.parse(body);
        var glbDetails = response.data.associate.glbList;
        var glbResponse = {};
        glbResponse['messageType'] = 'glbDetails';
        glbResponse['glbList'] = glbDetails;
        callback(glbResponse);
    })
    
}


