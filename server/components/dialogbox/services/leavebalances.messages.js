

'use strict';

/**
 * Rest Client Module to make calls to GV 
 */
const restClient = require('../../../components/rest');
/**
 * Constants which contain GV urls
 */
const constants = require('../../../components/constants');


/**
 * Method to get leave user balance by calling rest services
 * @param {*} requestParams Request Parameters to be sent with the request
 * @param {*} responseAPIAI Response from DialogFlow, to use parameters and intents from it
 * @param {*} callback Callback method to send response
 */
module.exports.getLeaveBalanceMessages = function(requestParams, responseAPIAI, callback) {
    requestParams['path'] = constants.LEAVE_BALANCES.replace('{{0}}', requestParams.id);

    restClient.getGVAPIResponse(requestParams, function(body){
        var arrayValues = JSON.parse(body);
        var leaveBalancesResArray = arrayValues.data.associate.leaveBalances;
        var response = {};
        if( responseAPIAI != undefined && responseAPIAI['parameters'] != undefined &&  Object.keys(responseAPIAI['parameters']).length > 0){
            logger.debug(leaveBalancesResArray);
            //logger.log(leaveBalancesResArray);
            response['messageType'] = 'leaveBalance';
            if(responseAPIAI['parameters']['PL'] && (responseAPIAI['parameters']['PL'] == 'PL' || responseAPIAI['parameters']['PL'] != '')){
                response['leaveType'] = 'PL';
            }
            else if(responseAPIAI['parameters']['CL'] && (responseAPIAI['parameters']['CL'] == 'CL'  || responseAPIAI['parameters']['CL'] != '')){
                response['leaveType'] = 'CL';
            }
            else if(responseAPIAI['parameters']['OH'] && (responseAPIAI['parameters']['OH'] == 'OH'  || responseAPIAI['parameters']['OH'] != '')){
                response['leaveType'] = 'OH';
            }
            else if(responseAPIAI['parameters']['CAL'] && (responseAPIAI['parameters']['CAL'] == 'CAL'  || responseAPIAI['parameters']['CAL'] != '')){
                response['leaveType'] = 'CAL';
            }
            else {
                response['messageType'] = 'leaveBalance';
                response['leaveBalances'] = leaveBalancesResArray;
            }

            if(response['leaveType'] != undefined){
                //response['leaveBalances'] = leaveBalancesResArray;
                if(response['leaveType'] == 'CAL'){
                    var total = 0;
                    leaveBalancesResArray.forEach(element => {
                        total+=element.totalCarryForward;
                    });
                    var obj = {};
                        obj.leaveName = 'Carried Forward Leave';
                        obj.totalBalance = total;
                    response['leaveBalances'] = obj;
                }
                else {
                    var filtered=leaveBalancesResArray.filter(function(item){
                        return item.leaveName == response['leaveType'];         
                    });
                    response['leaveBalances'] = filtered[0];
                }
            }
        }
        else {
            response['messageType'] = 'leaveBalance';
            response['leaveBalances'] = leaveBalancesResArray;
        }
        return callback(response);
    })
}