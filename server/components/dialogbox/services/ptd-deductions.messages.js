

'use strict'

/**
 * Rest Client Module to make calls to GV 
 */
const restClient = require('../../../components/rest');
/**
 * Constants which contain GV urls
 */
const constants = require('../../../components/constants');

/**
 * Method to get PTD deductions of an associate
 * @param {*} requestParams Request Parameters to be sent with the request
 * @param {*} responseAPIAI Response from DialogFlow, to use parameters and intents from it
 * @param {*} callback Callback method to send response
 */
module.exports.getPTDDeductions = function(requestParams, responseAPIAI, callback) {
    requestParams['country'] = 'IN';
    var url  = constants.PTD_DEDUCTIONS_URL;
   // url = url.replace('{{0}}', requestParams.id).replace('{{code}}', '1001').replace('{{country}}', 'IN');
   // url = url.replace('{{0}}', '414904');
    var deductionType = responseAPIAI.parameters.ptdDeductions;
    logger.log(responseAPIAI);
    //if(process.env.NODE_ENV === 'production'){ 
        url = url.replace('{{0}}', requestParams.id);
    //}
    //else { // for test and development purpose dummy data
    //    url = url.replace('{{0}}', '40001733_709');
    //}
    url = url.replace('{{0}}', requestParams.id);
    url = url.replace('{{codeName}}', constants.PTD_DEDUCTIONS_CODES[deductionType]);
    url = url.replace('{{country}}', 'IN');
    url = url.replace('{{endDate}}', getFormattedDate());
    logger.log(url);
    requestParams.sub = requestParams.id;
    requestParams.path = url;
    restClient.getGVAPIResponse(requestParams, function(body){
        var response = JSON.parse(body);
        try { 
            var message = {}
            message['messageType'] = 'ptdDeductions';
            message['body'] =   response.data[0];
            message['deductionType'] = deductionType;
            logger.log("PTD deductions")
            callback(message);
        }
        catch( e ){
            callback("Sorry, I cannot find any data");
        }
    })
}

/**
 * Method to get current date in required format for GV YYYYMMDD
 */
function getFormattedDate(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    } 
    if (mm < 10) {
        mm = '0' + mm;
    } 
    var today = yyyy +  mm +  dd;
    return today;
}