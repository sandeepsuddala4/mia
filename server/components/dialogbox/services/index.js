module.exports = {
    ...require('./flexi-benifits.messages'),
    ...require('./fpr-ratings.messages'),
    ...require('./glbdues.messages'),
    ...require('./gtl-nominees.messages'),
    ...require('./hierarchy.messages'),
    ...require('./leavebalances.messages'),
    ...require('./matchingquestions.message'),
    ...require('./mediclaimdetails.messages'),
    ...require('./pay-details.messages'),
    ...require('./personal-details.messages'),
    ...require('./ptd-deductions.messages')
  };


