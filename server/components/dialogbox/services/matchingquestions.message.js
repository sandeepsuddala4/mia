'use srtict';


const config = require('../../../config/environment');
/**
 * Python bridge plugin to generate matching questions
 */
var pythonbridge = require("../../../components/questionMatcher/pyhton-tfidf-bridge");
pythonbridge.init();


/**
 * Method to get matching question using python bridge
 * @param {*} question Question from user which has to be matched
 * @param {*} verifyCounterQuestionFlag counter question  flag
 * @param {*} callback Call back function
 */
module.exports.getMatchingQuestionsOnDefaultFallBack = function(question, verifyCounterQuestionFlag , callback) {
    var userQuestion = question;
        var incomingPhrases = [];
        logger.log("Python questions "+userQuestion)
        var matchedQuestions = pythonbridge.getTopMatchingQuestions(userQuestion, 5);
        logger.log(matchedQuestions)
        if (matchedQuestions && matchedQuestions.length > 0) {
           logger.log("Matched Questions....")
          verifyCounterQuestionFlag = false;
          matchedQuestions.forEach(element => {
            if (userQuestion !== element.phrase) {
               logger.log(element.phrase, "weight", element.weight);
              var characterElementValue = element.phrase.indexOf("?");
              var questionText;
              if (characterElementValue == -1) {
                  questionText = element.phrase + "?"
              } else {
                  questionText = element.phrase;
              }
              incomingPhrases.push(
                questionText
              );
            }
          });
          var uniqueArray = incomingPhrases.filter(function(elem, index, self) {
              return index === self.indexOf(elem);
          })
          uniqueArray.push("None of the above");
          callback(uniqueArray);
        } else {
          verifyCounterQuestionFlag = true;
          // logger.log("No matching question found");
          callback( config.defaultIntentFallBackQuestion);
        }
};
