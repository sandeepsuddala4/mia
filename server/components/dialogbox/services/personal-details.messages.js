/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-13 12:58:32 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-04-01 11:15:40
 */

'use strict'

/**
 * Rest Client Module to make calls to GV 
 */
const restClient = require('../../../components/rest');
/**
 * Constants which contain GV urls
 */
const constants = require('../../../components/constants');

/**
 * Method to get associate details like PAN, UAN etc.
 * @param {*} requestParams Request Parameters to be sent with the request
 * @param {*} responseAPIAI Response from DialogFlow, to use parameters and intents from it
 * @param {*} callback Callback method to send response
 */
module.exports.getAssociatePersonalDetails = function(requestParams, responseAPIAI, callback) {
    requestParams['code'] = responseAPIAI.parameters.Personal_Identifiers;
    requestParams['country'] = 'IN';
    requestParams['path'] = constants.ASSOCIATE_PERSONAL_DETAILS.replace('{{0}}', requestParams.id).replace('{{code}}', requestParams.code).replace('{{country}}', requestParams['country']);

    restClient.getGVAPIResponse(requestParams, function(body){
        var response = JSON.parse(body);
        var message = {}
        message['messageType'] = 'personalDetails';
        var details = []
        if(response != undefined && response['data'] != undefined && response['data'].length > 0){
            message['type'] = responseAPIAI.parameters.Personal_Identifiers;
            if(responseAPIAI.parameters.Personal_Identifiers != 'ALL'){
                var filtered=response['data'].filter(function(item){
                    return item.id == message['type'];         
                });
                message['data'] = filtered[0];                
            }
            else {
                message['data'] = response['data'];
            }
        }
        callback(message);
    })
}