/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-13 13:01:27 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-03-18 15:35:39
 */


'use strict'

/**
 * Rest Client Module to make calls to GV 
 */
const restClient = require('../../../components/rest');
/**
 * Constants which contain GV urls
 */
const constants = require('../../../components/constants');

/**
 * Method to get payment details of an associate
 * @param {*} requestParams Request Parameters to be sent with the request
 * @param {*} responseAPIAI Response from DialogFlow, to use parameters and intents from it
 * @param {*} callback Callback method to send response
 */
module.exports.getPayDetails = function(requestParams, responseAPIAI, callback){
    requestParams.sub = requestParams.id;
    logger.log(requestParams);
    restClient.getPayDetails(requestParams, function(body){
        var payStatement ;
        try {
            payStatement = JSON.parse(body.trim());
            var otherPays = payStatement.payStatement.otherPay;
            if(responseAPIAI.parameters.salaryDeductions == 'IT'){
                var deductions = otherPays.filter(pay => pay.sectionLabelName === 'Deductions');
                var incomeTaxDeductions = deductions[0].otherPayDetail.filter( deduction => deduction.labelName === 'Income Tax');
                var message  =new Object({
                    messageType: "incomeTax",
                    incomeTax : incomeTaxDeductions[0]
                });
                callback(message);
            }

            if(responseAPIAI.parameters.salaryDetails != undefined &&responseAPIAI.parameters.salaryDetails === 'Basic Salary') {
                var earnings = otherPays.filter(pay => pay.sectionLabelName === 'Earnings & Allowances');
                var basicSalaryObject = earnings[0].otherPayDetail.filter( pay => pay.labelName === 'Basic Salary');
                var message  =new Object({
                    messageType: "basicSalary",
                    basicSalary : basicSalaryObject[0]
                });
                callback(message);
            }

            if(responseAPIAI.parameters.salaryDetails != undefined &&responseAPIAI.parameters.salaryDetails === 'Total Deductions') {
                var deductions = otherPays.filter(pay => pay.sectionLabelName === 'Deductions');
                if(responseAPIAI.parameters['time-period'] === 'month') {
                    var message  =new Object({
                        messageType: "deductions",
                        deductions : deductions[0].otherPayDetail, 
                        timePeriod : "Month" 
                    });
                    callback(message);
                }
                else {
                    var message  =new Object({
                        messageType: "deductions",
                        deductions : deductions[0].otherPayDetail, 
                        timePeriod : "Year" 
                    });
                    callback(message);
                }
            }
        } 
        catch( e ){
            logger.log(e);
            callback("Sorry!! Unable to find the details");
        }
        //callback(body);
    })
}