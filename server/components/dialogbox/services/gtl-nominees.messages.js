
'use strict'


/**
 * Rest Client Module to make calls to GV 
 */
const restClient = require('../../../components/rest');
/**
 * Constants which contain GV urls
 */
const constants = require('../../../components/constants');

/**
 * Method to get GTL nominees list of an associate
 * @param {*} requestParams Request Parameters to be sent with the request
 * @param {*} responseAPIAI Response from DialogFlow, to use parameters and intents from it
 * @param {*} callback Callback method to send response
 */
module.exports.getAssociateGTLNominees = function(requestParams, responseAPIAI, callback) {
    requestParams['path'] = constants.GTL_NOMINEES.replace('{{0}}', requestParams.id);
    restClient.getGVAPIResponse(requestParams, function(body){
       logger.debug(body);
       body = JSON.parse(body);
       var associateDetails = body.data.associate;
       associateDetails['messageType'] = "gtlNominees";
       callback(associateDetails);
    })
}