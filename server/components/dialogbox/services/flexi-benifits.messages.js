/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-13 11:46:34 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-04-03 15:30:34
 */

'use strict'

/**
 * Rest Client Module to make calls to GV 
 */
const restClient = require('../../../components/rest');
/**
 * Constants which contain GV urls
 */
const constants = require('../../../components/constants');

/**
 * Method to get flexible benifits of an associate and their renewal date
 * @param {*} requestParams Request Parameters to be sent with the request
 * @param {*} responseAPIAI Response from DialogFlow, to use parameters and intents from it
 * @param {*} callback Callback method to send response
 */
module.exports.getAssociateFlexibleBenifits = function(requestParams, responseAPIAI, callback) {
    
    requestParams['country'] = 'IN';
    requestParams['code'] = '1001';
    requestParams['path'] = constants.ASSOCIATE_CLAIMS.replace('{{0}}', requestParams.id).replace('{{code}}', '1001').replace('{{country}}', 'IN');

    restClient.getGVAPIResponse(requestParams, function(body){
        var response = JSON.parse(body);
        logger.debug(body);
        var fbResponse = {};
        fbResponse['messageType'] = 'FBE'; // Flexible Benifit Eligibility
        if(response.data != undefined && response.data.length > 0){
            fbResponse['data'] = response.data;
        }
        else {
            fbResponse['data'] = [];
        }
        if(responseAPIAI.parameters != undefined && responseAPIAI.parameters.FlexibleBenefits != undefined && responseAPIAI.parameters.FlexibleBenefits != ''){
            fbResponse['type'] = responseAPIAI.parameters.FlexibleBenefits;
        }
        if(responseAPIAI.parameters != undefined && responseAPIAI.parameters.AmountType != undefined && responseAPIAI.parameters.AmountType == 'left'){
            fbResponse['type'] = 'remaining';
        }
        callback(fbResponse);
    })
    
}
