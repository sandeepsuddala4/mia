'use strict'

/**
 * Rest Client Module to make calls to GV 
 */
const restClient = require('../../../components/rest');
/**
 * Constants which contain GV urls
 */
const constants = require('../../../components/constants');

/**
 * Method to get mediclaim details of associate
 * @param {*} requestParams Request Parameters to be sent with the request
 * @param {*} responseAPIAI Response from DialogFlow, to use parameters and intents from it
 * @param {*} callback Callback method to send response
 */
module.exports.getAssociateMediClaimDetails = function(requestParams, responseAPIAI, callback) {
    requestParams['path'] = constants.MEDICLAIM_DETAILS.replace('{{0}}', requestParams.id);
    restClient.getGVAPIResponse(requestParams, function(body){
        logger.debug(body);
        var response = JSON.parse(body);
        var dependents = response.data.associate.dependents;
        var messageType ="mediclaimNominees";
        var response = {};
        response['messageType'] = messageType;
        response['dependents'] = dependents;
        callback(response);
    })
}