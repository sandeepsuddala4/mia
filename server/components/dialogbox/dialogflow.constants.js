const dialogFlowService = require('./services')

const intentNames = {
    leaveIntentName: 'What is my leave balance?',
    hierarchyIntentName: 'Associate Hierarchy',
    mediClaimDetailsIntentName: 'Mediclaim Details', 
    gtlNomineesIntentName: 'GTL Nominees',
    fprRatingIntentName: 'FPR Rating',
    glbDuesIntentName: 'GLB due',
    flexibleBenifitsIntentName : 'FlexibleBenifitDetails',
    salaryDetailsIntentName: 'Salary Details',
    ptdDeductionIntentName: 'PTDDeductions',
    personalIdentifiersIntentName: 'Personal Identifiers'
}

module.exports.methodName = {
    [intentNames.leaveIntentName] :  dialogFlowService.getLeaveBalanceMessages,
    [intentNames.hierarchyIntentName]: dialogFlowService.getAssociateHeirarchyDetails, 
    [intentNames.mediClaimDetailsIntentName]: dialogFlowService.getAssociateMediClaimDetails,
    [intentNames.gtlNomineesIntentName]: dialogFlowService.getAssociateGTLNominees,
    [intentNames.fprRatingIntentName]:dialogFlowService.getAssociateFPRRatings,
    [intentNames.glbDuesIntentName]: dialogFlowService.getAssociateGLBDues,
    [intentNames.flexibleBenifitsIntentName]: dialogFlowService.getAssociateFlexibleBenifits,
    [intentNames.salaryDetailsIntentName]: dialogFlowService.getPayDetails,
    [intentNames.ptdDeductionIntentName]: dialogFlowService.getPTDDeductions,
    [intentNames.personalIdentifiersIntentName]:dialogFlowService.getAssociatePersonalDetails,

}
