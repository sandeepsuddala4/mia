/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-04 10:25:31 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-03-27 19:02:25
 */

'use strict'
/**
 * Environmental Configurations
 */
const config = require('../../config/environment');
/**
 * DialogFlow Service
 */
const dialogFlowService = require('./services');

/**
 * Logs Controller
 */
const logsController = require('../../api/logs/logs.controller');

const constants = require('./dialogflow.constants');

var options = { };

var smallTalk, verifyCounterQuestionFlag = false;



if(config.requiredProcessEnv('NODE_ENV') !== 'production'){
    options.proxyHost =  config.proxy.url,
    options.proxyPort =  config.proxy.port
}

module.exports.getDialogFlowResponse = function(apiAIApp, socket, message){
    var associateId = socket.decoded.associateId;
    options.sessionId = associateId;
    var associateName = socket.decoded.associateName;

    const apiAIrequest = apiAIApp.textRequest(message, options);
    //logger.log(options)
    apiAIrequest.on('response', function (response) {
        smallTalk = response.result.action.split(".")[0];
        var defaultFallback = response.result.metadata.intentName;
        logger.log("inents sss "+response.result.metadata.intentName)
        logger.log(response.result);
        var body = new Object({
          question: message,
          associateId: associateId,
          associateName: associateName
        });

        if (response.result.metadata.intentName === config.defaultFallBackIntentName) {
            var counterQuestion = socket.counterQuestion == undefined ? 0 : socket.counterQuestion;
          if (verifyCounterQuestionFlag) {
            socket.counterQuestion = counterQuestion++;
          } else {
            socket.counterQuestion = 0;
          }
          if (socket.counterQuestion === config.counterQuestionLimit) {
            socket.emit('chat message', 'HR: ' + config.defaultIntentFallBackQuestionUnableToAnswer, smallTalk, defaultFallback);
            counterQuestion = 1;
            body.action = 'default fallback';
          } else {
            body.action = 'Options';
            dialogFlowService.getMatchingQuestionsOnDefaultFallBack(message,verifyCounterQuestionFlag ,function(questionsData){
              logger.log('emiting message')
              socket.emit('chat message', 'HR: ' + questionsData, smallTalk, defaultFallback);
            });
            
          }
        } else {
          socket.counterQuestion = 1;
          counterQuestion = 1;          
          body.action= 'answered'
          
          if(defaultFallback === 'Default Fallback Intent'){
            socket.emit('chat message', 'HR: ' + response.result.fulfillment.speech, smallTalk, defaultFallback);
          }
          else {
            processMessage(response, socket);
          } 
        }
        try {
          logsController.save(body);
        }
        catch(e){
          logger.error("",e);
        }
        
    });

    apiAIrequest.on('error', function(error) {
      logger.error(error);
      socket.emit('chat message', 'HR: ' + 'Looks like I have trouble in contacting the server. Please try again later !!!');
    });
     
    apiAIrequest.end();
}

function processMessage( response, socket){
  var defaultFallback = response.result.metadata.intentName;
  var associateId = socket.decoded.associateId;
  var intentName = response.result.metadata.intentName;
  if(intentName !== undefined && typeof (constants.methodName[intentName]) === "function" ){
    var func = constants.methodName[intentName];
    var requestParams= {};
    requestParams.id = associateId;
    requestParams.sub = associateId;
    func(requestParams, response.result,function(msg){
        var msg =JSON.stringify(msg);
        socket.emit('chat message', 'HR: ' + msg, smallTalk, defaultFallback);
    }); 
  }
  else {
      socket.emit('chat message', 'HR: ' + response.result.fulfillment.speech, smallTalk, defaultFallback);
  }
}