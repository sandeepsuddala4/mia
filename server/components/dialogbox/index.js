
/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-04 10:26:01 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-03-12 15:35:06
 */

'use strict'; 

var apiai ;
const config = require('../../config/environment');
const constants = require('../constants');

if(config.requiredProcessEnv('NODE_ENV') == 'production' ){
    apiai = require('apiai');
}
else {
    apiai = require('apiai-proxy');
}

const apiaiApp = apiai(config.requiredProcessEnv('APIAI_CLIENT_ACCESS_TOKEN'));

module.exports = apiaiApp;