'use strict'
const config = require('../../config/environment');
const restClient = require('../../components/rest');
const constants = require('../../components/constants');

var pythonbridge = require("../../components/questionMatcher/pyhton-tfidf-bridge");
pythonbridge.init();





module.exports.getMatchingQuestionsOnDefaultFallBack = function(question, verifyCounterQuestionFlag , callback) {
    var userQuestion = question;
        var incomingPhrases = [];
        console.log("Python questions "+userQuestion)
        var matchedQuestions = pythonbridge.getTopMatchingQuestions(userQuestion, 5);
        console.log(matchedQuestions)
        if (matchedQuestions && matchedQuestions.length > 0) {
           console.log("Matched Questions....")
          verifyCounterQuestionFlag = false;
          matchedQuestions.forEach(element => {
            if (userQuestion !== element.phrase) {
               console.log(element.phrase, "weight", element.weight);
              var characterElementValue = element.phrase.indexOf("?");
              var questionText;
              if (characterElementValue == -1) {
                  questionText = element.phrase + "?"
              } else {
                  questionText = element.phrase;
              }
              incomingPhrases.push(
                questionText
              );
            }
          });
          var uniqueArray = incomingPhrases.filter(function(elem, index, self) {
              return index === self.indexOf(elem);
          })
          uniqueArray.push("None of the above");
          callback(uniqueArray);
        } else {
          verifyCounterQuestionFlag = true;
          // console.log("No matching question found");
          callback( config.defaultIntentFallBackQuestion);
        }
};



/**
 * Method to get leave user balance by calling rest services
 * @param {*} requestParams 
 * @param {*} responseAPIAI response.result of api ai
 * @param {*} callback 
 */
module.exports.getLeaveBalanceMessages = function(requestParams, responseAPIAI, callback) {
    requestParams['path'] = constants.LEAVE_BALANCES.replace('{{0}}', requestParams.id);

    restClient.getGVAPIResponse(requestParams, function(body){
        var arrayValues = JSON.parse(body);
        var leaveBalancesResArray = arrayValues.data.associate.leaveBalances;
        var leaveBalancesArray;
        
        var response = {};
        if( responseAPIAI != undefined && responseAPIAI['parameters'] != undefined &&  Object.keys(responseAPIAI['parameters']).length > 0){
            logger.debug(leaveBalancesResArray);
            //console.log(leaveBalancesResArray);
            response['messageType'] = 'leaveBalance';
            if(responseAPIAI['parameters']['PL'] == 'PL' || responseAPIAI['parameters']['PL'] != ''){
                response['leaveType'] = 'PL';
            }
            else if(responseAPIAI['parameters']['CL'] == 'CL'  || responseAPIAI['parameters']['CL'] != ''){
                response['leaveType'] = 'CL';
            }
            else if(responseAPIAI['parameters']['OH'] == 'OH'  || responseAPIAI['parameters']['OH'] != ''){
                response['leaveType'] = 'OH';
            }
            else if(responseAPIAI['parameters']['CAL'] == 'CAL'  || responseAPIAI['parameters']['CAL'] != ''){
                response['leaveType'] = 'CAL';
            }
            else {
                response['messageType'] = 'leaveBalance';
                response['leaveBalances'] = leaveBalancesResArray;
            }

            if(response['leaveType'] != undefined){
                //response['leaveBalances'] = leaveBalancesResArray;
                if(response['leaveType'] == 'CAL'){
                    var total = 0;
                    leaveBalancesResArray.forEach(element => {
                        total+=element.totalCarryForward;
                    });
                    var obj = {};
                        obj.leaveName = 'Carried Forward Leave';
                        obj.totalBalance = total;
                    response['leaveBalances'] = obj;
                }
                else {
                    var filtered=leaveBalancesResArray.filter(function(item){
                        return item.leaveName == response['leaveType'];         
                    });
                    response['leaveBalances'] = filtered[0];
                }
            }
        }
        else {
            response['messageType'] = 'leaveBalance';
            response['leaveBalances'] = leaveBalancesResArray;
        }
        return callback(response);
    })
}



module.exports.getAssociateHeirarchyDetails = function(requestParams, responseAPIAI, callback) {
    requestParams['path'] = constants.ASSOCIATE_HEIRARCHY_DETAILS.replace('{{0}}', requestParams.id);
    //logger.debug("inside getAssociateHeirarchyDetails")
    restClient.getGVAPIResponse(requestParams, function(body){
        console.log('inside getAssociateHeirarchyDetails')
        console.log(body)
        logger.debug(body);
        body = JSON.parse(body);
        var reportingHeirarchy = body.data.associate.reportingHeirarchy;
        var index = Object.keys(reportingHeirarchy).length;
            var message ='';
            message = 'Take a look at your heirarchy';
            if(reportingHeirarchy){
                var margin = 0;
                var list =''
                while(index >= 1){
                    console.log(reportingHeirarchy[index+""])
                    var data = reportingHeirarchy[index+""];
                    var mm = '<li style="list-style-type:disc;margin-left:'+margin+'px">'+data.firstName +' '+data.lastName+'</li>'
                    list+= mm;
                    margin = margin+5;
                    index = index -1;
                }
                message += '<ul>'+list+'</ul>';
                callback(message);
            }
    })
}



module.exports.getAssociateMediClaimDetails = function(requestParams, responseAPIAI, callback) {
    requestParams['path'] = constants.MEDICLAIM_DETAILS.replace('{{0}}', requestParams.id);
    //logger.debug("inside getAssociateHeirarchyDetails")
    restClient.getGVAPIResponse(requestParams, function(body){
        logger.debug(body);
        var response = JSON.parse(body);
        var dependents = response.data.associate.dependents;
        var messageType ="mediclaimNominees";
        var response = {};
        response['messageType'] = messageType;
        response['dependents'] = dependents;
        callback(JSON.stringify(response));
    })
}


module.exports.getAssociateGTLNominees = function(requestParams, responseAPIAI, callback) {
    requestParams['path'] = constants.GTL_NOMINEES.replace('{{0}}', requestParams.id);
    restClient.getGVAPIResponse(requestParams, function(body){
       logger.debug(body);
       body = JSON.parse(body);
       var associateDetails = body.data.associate;
       associateDetails['messageType'] = "gtlNominees";
       callback(associateDetails);
    })
}


module.exports.getAssociateFPRRatings = function(requestParams, responseAPIAI, callback) {
    requestParams['path'] = constants.FPR_RATINGS.replace('{{0}}', requestParams.id);
    restClient.getGVAPIResponse(requestParams, function(body){
        logger.debug(body);
        var response = JSON.parse(body);
        var ratingResponse = {};
        ratingResponse.messageType = 'fprRatings'
        try {
            ratingResponse.appraisalList = response.data.associate.appraisalList;
        }
        catch(e){
            ratingResponse.appraisalList = [];
        }
        callback(ratingResponse);
    })
    
}


module.exports.getAssociateGLBDues = function(requestParams, responseAPIAI, callback) {
    requestParams['path'] = constants.GLB_DETAILS.replace('{{0}}', requestParams.id);
    restClient.getGVAPIResponse(requestParams, function(body){
        logger.log(body);
        var response = JSON.parse(body);
        var glbDetails = response.data.associate.glbList;
        var glbResponse = {};
        glbResponse['messageType'] = 'glbDetails';
        glbResponse['glbList'] = glbDetails;
        callback(glbResponse);
    })
    
}


module.exports.getAssociateFlexibleBenifits = function(requestParams, responseAPIAI, callback) {
    requestParams['path'] = constants.ASSOCIATE_CLAIMS.replace('{{0}}', requestParams.id).replace('{{code}}', '1001').replace('{{country}}', 'IN');

    restClient.getGVAPIResponse(requestParams, function(body){
        var response = JSON.parse(body);
        logger.debug(body);
        var fbResponse = {};
        fbResponse['messageType'] = 'FBE'; // Flexible Benifit Eligibility
        if(response.data != undefined && response.data.length > 0){
            fbResponse['data'] = response.data;
        }
        else {
            fbResponse['data'] = [];
        }
        if(responseAPIAI.parameters != undefined && responseAPIAI.parameters.FlexibleBenefits != undefined && responseAPIAI.parameters.FlexibleBenefits != ''){
            fbResponse['type'] = responseAPIAI.parameters.FlexibleBenefits;
        }
        callback(fbResponse);
    })
    
}


module.exports.getAssociatePersonalDetails = function(requestParams, responseAPIAI, callback) {
    requestParams['path'] = constants.ASSOCIATE_PERSONAL_DETAILS.replace('{{0}}', requestParams.id).replace('{{code}}', '1001').replace('{{country}}', 'IN');

    restClient.getGVAPIResponse(requestParams, function(body){
        var response = JSON.parse(body);
        var message = {}
        message['messageType'] = 'personalDetails';
        var details = []
        if(response != undefined && response['data'] != undefined && response['data'].length > 0){
            message['type'] = responseAPIAI.parameters.Personal_Identifiers;
            if(responseAPIAI.parameters.Personal_Identifiers != 'ALL'){
                var filtered=response['data'].filter(function(item){
                    return item.id == message['type'];         
                });
                message['data'] = filtered[0];                
            }
            else {
                message['data'] = response['data'];
            }
        }
        callback(message);
    })
}


module.exports.getPayDetails = function(requestParams, responseAPIAI, callback){
    requestParams.sub = requestParams.id;
    console.log(requestParams)
    restClient.getPayDetails(requestParams, function(body){
        var payStatement ;
        try {
            payStatement = JSON.parse(body);
            var otherPays = payStatement.payStatement.otherPay;
            if(responseAPIAI.parameters.salaryDeductions == 'IT'){
                var deductions = otherPays.filter(pay => pay.sectionLabelName === 'Deductions');
                var incomeTaxDeductions = deductions[0].otherPayDetail.filter( deduction => deduction.labelName === 'Income Tax');
                var message  =new Object({
                    messageType: "incomeTax",
                    incomeTax : incomeTaxDeductions[0]
                });
                callback(message);
            }

            if(responseAPIAI.parameters.salaryDetails != undefined &&responseAPIAI.parameters.salaryDetails === 'Basic Salary') {
                var earnings = otherPays.filter(pay => pay.sectionLabelName === 'Earnings & Allowances');
                var basicSalaryObject = earnings[0].otherPayDetail.filter( pay => pay.labelName === 'Basic Salary');
                var message  =new Object({
                    messageType: "basicSalary",
                    basicSalary : basicSalaryObject[0]
                });
                callback(message);
            }

            if(responseAPIAI.parameters.salaryDetails != undefined &&responseAPIAI.parameters.salaryDetails === 'Total Deductions') {
                var deductions = otherPays.filter(pay => pay.sectionLabelName === 'Deductions');
                if(responseAPIAI.parameters['time-period'] === 'month') {
                    var message  =new Object({
                        messageType: "deductions",
                        deductions : deductions[0].otherPayDetail, 
                        timePeriod : "Month" 
                    });
                    callback(message);
                }
                else {
                    var message  =new Object({
                        messageType: "deductions",
                        deductions : deductions[0].otherPayDetail, 
                        timePeriod : "Year" 
                    });
                    callback(message);
                }
            }
        } 
        catch( e ){
            console.log(e);
            callback("Error while retrieving your data");
        }
        //callback(body);
    })
}



module.exports.getPTDDeductions = function(requestParams, responseAPIAI, callback) {
    var url  = constants.PTD_DEDUCTIONS_URL;
   // url = url.replace('{{0}}', requestParams.id).replace('{{code}}', '1001').replace('{{country}}', 'IN');
   // url = url.replace('{{0}}', '414904');
    var deductionType = responseAPIAI.parameters.ptdDeductions;
    console.log(responseAPIAI)
    url = url.replace('{{codeName}}', constants.PTD_DEDUCTIONS_CODES[deductionType]);
    url = url.replace('{{country}}', 'IN');
    url = url.replace('{{endDate}}', getFormattedDate());
    console.log(url);
    requestParams.sub = requestParams.id;
    requestParams.path = url;
    restClient.getGVAPIResponse(requestParams, function(body){
        var response = JSON.parse(body);
        try { 
            var message = {}
            message['messageType'] = 'ptdDeductions';
            message['body'] =   response.data[0];
            message['deductionType'] = deductionType;
            console.log("PTD deductions")
            callback(message);
        }
        catch( e ){
            callback("Sorry, I cannot find any data");
        }
    })
}

function getFormattedDate(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
    dd = '0' + dd;
    } 
    if (mm < 10) {
    mm = '0' + mm;
    } 
    var today = yyyy +  mm +  dd;
    return today;
}