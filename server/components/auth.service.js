'use strict';

var config = require('../config/environment');
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var compose = require('composable-middleware');
const encryptor = require('./util/crypto');
var url = require('url');
const jwtDB = require('../api/auth/jwt.db');
const verifyOptions = {
    "alg": "HS256",
    "typ": "JWT"
}
/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */
function isAuthenticated() {
  return compose()
    // Validate jwt
    .use(function(req, res, next) {
        logger.log(req.body);
      // allow access_token to be passed through query parameter as well
      if(req.query && req.query.hasOwnProperty('token')) {
            req.headers.authorization = 'Bearer ' + req.query.token;
      }
    let token = req.headers['x-access-token'] || req.headers['authorization'] || req.query.token; // Express headers are auto converted to lowercase
        if (token && token.startsWith('Bearer ')) {
            // Remove Bearer from string
            token = token.slice(7, token.length);
        }
        if (token) {
            jwt.verify(token, config.requiredProcessEnv('JWT_SECRET'),verifyOptions, (err, decoded) => {
            if (err) {
                return res.render('401');
            } else {
                var referer = req.headers.referer;
                var userDetails = encryptor.decryptUserDetails(decoded.apd);
                jwtDB.isValidToken(new Object( {associateId: userDetails.associateId, token: token}), function(isValid){
                    logger.log('--------------userDetails----------------------');
                    logger.log(userDetails);
                    
                    if(referer && isValid){
                        var u = url.parse(referer);
                        if(err){
                            return res.render('401');
                        } 
                        else if(!u ||  config.allowedReferers.indexOf(u.host) === -1) {
                            return res.render('401');
                        }
                        if(userDetails.refUrl.indexOf(u.host) === -1){
                            return  res.render('401');
                        }
                    
                        req.decoded = userDetails;
                        next();
                    }
                    else {
                        return res.render('401');
                    }
                });
                
            }
            });
        } else {
            return res.render('401');
        }
    });
}


/**
 * Returns a jwt token signed by the app secret
 */
function signToken(id) {
  return jwt.sign({ _id: id }, config.secrets.session, { expiresInMinutes: 60*5 });
}

/**
 * Set token cookie directly for oAuth strategies
 */
function setTokenCookie(req, res) {
  if (!req.data) return res.json(404, { message: 'Something went wrong, please try again.'});
  var token = signToken(req.user._id, req.user.role);
  res.cookie('token', JSON.stringify(token));
  res.redirect('/');
}

function decodeJWT(token){
   var decodedObj =  jwt.verify(token, env.requiredProcessEnv('JWT_SECRET'));
   return decodedObj;
}


exports.isAuthenticated = isAuthenticated;
exports.signToken = signToken;
exports.setTokenCookie = setTokenCookie;