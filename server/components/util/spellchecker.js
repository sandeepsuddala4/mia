var translate = require('google-translate-api');
exports.userSpellingCorrection = function (userSpellingCorrection, callback) {
    translate(userSpellingCorrection, {to: 'en'}).then(res => {
        console.log(res);
        callback(false, res.text);
    }).catch(err => {
        console.error(err);
        callback(true, null); 
    });
};