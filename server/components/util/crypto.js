const crypto = require('crypto');
const algorithm = 'aes-256-cbc';
const key = crypto.randomBytes(32);
const iv = crypto.randomBytes(16);

const delimtter = '$pda$';
function encrypt(text) {
    let cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
}
   
   function decrypt(text) {
    let iv = Buffer.from(text.iv, 'hex');
    let encryptedText = Buffer.from(text.encryptedData, 'hex');
    let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
   }
   

   function encryptUserDetails(userObj){
       var encrypted = encrypt(JSON.stringify(userObj));
       return encrypted.iv+delimtter+encrypted.encryptedData;
   }

   function decryptUserDetails(encryptedDetails){
        var encrypted = new Object({
            iv: encryptedDetails.split(delimtter)[0],
            encryptedData: encryptedDetails.split(delimtter)[1]
        });
        var decrypted = decrypt(encrypted);

        return JSON.parse(decrypted);
   }

module.exports.encryptUserDetails = encryptUserDetails;
module.exports.decryptUserDetails = decryptUserDetails;
