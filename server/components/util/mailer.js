'use strict';

const nodemailer = require('nodemailer');
const config = require('../../config/environment');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: config.requiredProcessEnv('MIA_FEEDBACK_EMAIL_ADDRESS'),
        pass: config.requiredProcessEnv('MIA_FEEDBACK_EMAIL_PASSWORD')
    }
});

var sendEmail = function(subject,message,mailOptions) {
    mailOptions.subject = subject;
    mailOptions.html = message;
    if(config.sendEmail){
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) { 
                logger.error("Error while sending mail info - ",mailOptions);
                logger.error("Error while sending mail",error);
                return false;
            } else {
                return true;
            }
        });
    }
    else {
        return true;
    }
};


module.exports.sendMail = function(subject, message){
    var mailOptions = {
        from: config.requiredProcessEnv('MIA_FEEDBACK_EMAIL_ADDRESS'),
        to: config.supportEmails,
    };
    
    sendEmail(subject, message, mailOptions);
}


module.exports.sendFeedBackEmail = function(feedBackModel){
    var subject = 'Feedback of MIA';
    var html =  '<div>Hi,<br><br>{{message}}<br><br><span><b>Question:</b> {{questionAsked}}</span><br><br><span><b>Answer:</b> {{answer}}</span><br><br><span>Thanks</span><br>{{associateName}}</div>';
    html = html.replace("{{message}}", feedBackModel.feedbackMessage);
    html = html.replace("{{questionAsked}}", feedBackModel.userQuestion);
    html = html.replace("{{question}}", feedBackModel.question);
    html = html.replace("{{associateName}}", feedBackModel.associateName);
    sendEmail(subject,html, mailOptions);
}

module.exports.sendUnCaughtException = function(body){
    var mailOptions = {
        from: config.requiredProcessEnv('MIA_FEEDBACK_EMAIL_ADDRESS'),
        to: config.itSupportEmails,
    };
    var subject = "Uncaught Exception";
    sendEmail(subject, body, mailOptions);
}