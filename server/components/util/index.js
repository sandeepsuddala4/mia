const url = require('url');
const config = require('../../config/environment')

var getUrl = function(referer){
    var refUrl = url.parse(referer);
    if(refUrl){
        refUrl = refUrl.href.replace(refUrl.path, '');
        return refUrl;
    }
    return '';
}

module.exports.getHostUrl = function(referer){
    return getUrl(referer)
}

module.exports.isAllowedReferer = function(referer){
    var refUrl = getUrl(referer);
    var allowedReferers = config.allowedReferers;
    logger.log(refUrl)
    logger.log(allowedReferers.indexOf(refUrl.trim()))
    if(refUrl != undefined && allowedReferers.indexOf(refUrl.trim()) >= 0){
        return true;
    }
    else {
        return false; 
    }
}