'use strict'

var winston = require('winston');

var myCustomLevels = {
    levels: {
        debug:0,
        info: 1,
        silly:2,
        warn: 3,
        error:4,
    },
    colors: {
        debug: 'green',
        info:  'cyan',
        silly: 'magenta',
        warn:  'yellow',
        error: 'red'
    }
};

const myconsole = new winston.transports.Console();
const colorizer = winston.format.colorize({colors: myCustomLevels.colors});
const myFormat = winston.format.printf(({ timestamp, level, message, meta }) => {
    return `${timestamp} -- ${level} -- ${message} -- ${meta? JSON.stringify(meta) : ''}`;
  });
const alignedWithColorsAndTime = winston.format.combine(
    colorizer,
    winston.format.json(),
    winston.format.timestamp(),
    winston.format.align(),
    winston.format.colorize({
        all:true
    }),
    winston.format.label({
        label:'[LOGGER]'
    }),
    winston.format.timestamp({
        format:"YY-MM-DD HH:MM:SS"
    }),
    winston.format.printf(
        info => ` ${info.label}  ${info.timestamp}  ${info.level} : ${info.message} : ${info? JSON.stringify(info.message) : ''}`
    )
  );


  const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
      new winston.transports.File({ filename: 'error.log',maxsize: 1000000 , maxFiles:10, tailable:true, level: 'error' }),
      new winston.transports.File({ filename: 'combined.log',maxsize: 1000000,maxFiles:10, tailable:true })
    ]
  });
  
  
  if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        format: winston.format.combine(winston.format.colorize(), alignedWithColorsAndTime)
    }));
  }


module.exports.logger = winston;

module.exports.debug = function(message){
    logger.debug("", message)
}


module.exports.error = function(message){
    logger.error("",message)
}

module.exports.log = function(message){
    if(process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'test' ){
        console.log(message);
    }
}

module.exports.info = function(message, options){
    if(process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'test'){
        console.info( message, options);
    }
}
