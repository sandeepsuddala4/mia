const jwt = require('jsonwebtoken');
const fs = require('fs');
const uuidv1 = require('uuid/v1');
const path = require('path');
const config = require('../../config/environment');
var _ = require('lodash');

// var cert_file_path = "certs\\portal106.globalview.adp.com.crt";
// var pvt_key_file_path = "service\\certs\\portal106.globalview.adp.com.key";
// var public_key_file_path = "service\\certs\\portal106.globalview.adp.com-pubkey.pem";

// var pvt_key_file_path =  path.join(__dirname, '..', 'certs', 'portal106.globalview.adp.com.key');
// var public_key_file_path = path.join(__dirname, '..', 'certs', 'portal106.globalview.adp.com-pubkey.pem');


var pvt_key_file_path =  path.join(__dirname, '..', 'certs', config.GV_URL+'.key');
var public_key_file_path = path.join(__dirname, '..', 'certs', config.GV_URL+'-pubkey.pem');

exports.generateJWT = function (sub, forPay) {
    var public_key = fs.readFileSync(public_key_file_path, 'utf-8');
    var pvt_key = fs.readFileSync(pvt_key_file_path);
    
    if(public_key != undefined) {
       public_key = public_key.replace(/\n/g,' ')
    }
    
    var header = {
        "alg": "RS512",
        "typ": "JWT",
        "x5c": [public_key],
    }
    var jti = uuidv1();
    var iat = Math.floor(Date.now() / 1000);
    var exp = iat + 7200;
    var envPayload = {}
    if(forPay){
        envPayload = config.PAY_JWT_PARAMS;
    }
    else {
        envPayload = config.JWT_GENERATOR_PARAMS;
    }

    logger.log("-----------------------------------------------------Generate JWT---------------------------------------------------");

    var payload = {
        "sub": sub,
        "jti": jti,
        "iat": iat,
        "exp": exp
    }
    
    payload = _.merge(
        payload,
        envPayload || {});
        logger.debug(payload);
        
    var token = jwt.sign(payload, pvt_key, {
        algorithm: "RS512",
        header: header
    });

    return token;
}
