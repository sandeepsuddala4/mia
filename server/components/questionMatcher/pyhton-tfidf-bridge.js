exports = module.exports = {}

var tm = require('text-miner');
var HashMap = require('hashmap');

var constants = require('./constants');
var fileprovider = require('./file-provider');

// var fileVocabularyJsonPath = require('./vocabulary.json');
// var fileQuestionIndexPath = require('./question_index.json');
// var fileWeightsPath = require('./weights.json');

var pythonOutputDir = "python-ouput-data";
var vocabs_map = new HashMap();
var questions_map = new HashMap();
var weights = [];

var initialized = false;

exports.init = function () {

    if (initialized)
        return;
    //read word vocabulary with index
    // console.log(__dirname);

    var vocabFilePath = __dirname + "/" + "vocabulary.json";
    // console.log("vocabFilePath:",vocabFilePath)

    // var vocabFilePath = "D:\\ADP\\ADP-ChatBot\\adp-chatbot\\service\\python-output-data\\vocabulary.json";
    if (fileprovider.existsFile(vocabFilePath)) {
        var vocabs = JSON.parse(fileprovider.readFile(vocabFilePath));
        vocabs.forEach(element => {
            vocabs_map.set(element.word, element.word_index);
        });
        // console.log(vocabs);
    }

    //read questions with index
    var questionFilePath = __dirname + "/" + "question_index.json";
    // var questionFilePath = "D:\\ADP\\ADP-ChatBot\\adp-chatbot\\service\\python-output-data\\question_index.json";
    if (fileprovider.existsFile(questionFilePath)) {
        var questions = JSON.parse(fileprovider.readFile(questionFilePath));
        questions.forEach(element => {
            questions_map.set(element.question_index, element.question);
        });
    }

    //read weights
    var weightFilePath = __dirname + "/" + "weights.json";
    // var weightFilePath = "D:\\ADP\\ADP-ChatBot\\adp-chatbot\\service\\python-output-data\\weights.json";
    if (fileprovider.existsFile(weightFilePath)) {
        weights = JSON.parse(fileprovider.readFile(weightFilePath));
        // console.log(weights.length);
        // console.log(weights[0].length);
    }
    initialized = true;
}

exports.getTopMatchingQuestions = function (question, numbersToMatch = 5) {
    //first tokenize the phrase
    var tokens = tokenize(question);
    var combinedWeightMap = getCombinedWeights(tokens); //key is the question index

    var phraseCombinedWeights = [];
    combinedWeightMap.keys().forEach(key => {
        phraseCombinedWeights.push({
            phrase: questions_map.get(key),
            weight: combinedWeightMap.get(key)
        })
    });

    //sort the phrases in descending order of weight
    if (phraseCombinedWeights.length == 0) {
        console.log("No matching phrase found");
        return [];
    } else {
        var sortedMatchingPhrases = phraseCombinedWeights.sort(function (a, b) {
            return b.weight - a.weight
        });
        // console.log(sortedMatchingPhrases.length + " matching phrases found");
        //remove duplicates
        var temp = {};
        for (i = 0; i < sortedMatchingPhrases.length; i++) {
            temp[sortedMatchingPhrases[i]['phrase']] = sortedMatchingPhrases[i];
        }

        sortedMatchingPhrases = new Array();
        for (var key in temp) {
            sortedMatchingPhrases.push(temp[key]);
        }
        if (sortedMatchingPhrases.length >= numbersToMatch)
            return sortedMatchingPhrases.slice(0, numbersToMatch);
        else
            return sortedMatchingPhrases;
    }

}

tokenize = function (phrase) {
    var phrases = [];
    phrases.push(phrase);
    var phrase_corpus = new tm.Corpus(phrases);
    phrase_corpus.trim().
    toLower().
    clean().
    removeInterpunctuation().
    removeNewlines().
    removeWords(tm.STOPWORDS.EN).
    removeDigits();

    var termsMatrix = new tm.TermDocumentMatrix(phrase_corpus);
    return termsMatrix.vocabulary;
}

getCombinedWeights = function (tokens) {
    var weightMap = new HashMap(); //map of document index & combined weights

    tokens.forEach(token => {
        if (vocabs_map.get(token)) {
            var tokenIndex = vocabs_map.get(token);
            var tokenWeights = weights[tokenIndex];
            for (questionIndex = 0; questionIndex < tokenWeights.length; questionIndex++) {
                var questionWeight = tokenWeights[questionIndex];
                if (questionWeight > 0) {
                    var existingWeight = weightMap.get(questionIndex) ? weightMap.get(questionIndex) : 0;
                    weightMap.set(questionIndex, existingWeight + questionWeight);
                }
            }
        }
    });

    return weightMap;

}