var fs = require('fs')
var rimraf = require('rimraf')

exports = module.exports = {}

exports.readAllFiles = function (dirPath) {
    console.log(dirPath)
    var filenames = fs.readdirSync(dirPath);
    var fileContents = [];
    filenames.forEach(function (filename) {
        if (!filename.match(/.json$/))
            return;

        var filecontent = fs.readFileSync(dirPath + filename, 'utf-8');
        fileContents.push({
            "fileName": filename,
            "fileContent": filecontent
        });
    })


    return fileContents;
}

exports.writeFiles = function (dirPath, files) {
    if (fs.existsSync(dirPath)){
        rimraf.sync(dirPath);
    }
    
    fs.mkdirSync(dirPath);

    files.forEach(function (file) {
        fs.writeFileSync(dirPath + '/' + file.fileName, file.fileContent);
    })
}

exports.existsFile = function (path) {
    return fs.existsSync(path);
}

exports.readFile = function (path) {
    var filecontent = fs.readFileSync(path, 'utf-8');
    return filecontent;
}

exports.writeSingleFile = function (dirPath, file) {
    if (fs.existsSync(dirPath)){
        rimraf.sync(dirPath);
    }
    
    fs.mkdirSync(dirPath);

    fs.writeFileSync(dirPath + '/' + file.fileName, file.fileContent);
}