exports = module.exports = {}

var working_Dir = '';

exports.setWorkingDirectory = function(dirPath)
{
    working_Dir = dirPath;
}

exports.getWorkingDirectory = function()
{
    return working_Dir;
}