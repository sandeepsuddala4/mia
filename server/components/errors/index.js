/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-04 10:26:28 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-03-11 12:00:46
 */


/**
 * Error responses
 */

'use strict';

module.exports[404] = function pageNotFound(req, res) {
  var viewFilePath = '404';
  var statusCode = 404;
  var result = {
    status: statusCode
  };

  res.status(result.status);
  res.render(viewFilePath, function (err) {
    if (err) { return res.json(result, result.status); }

    res.render(viewFilePath);
  });
};

module.exports[500] = function internalError(req, res) {
  var viewFilePath = '500';
  var statusCode = 500;
  var result = {
    status: statusCode
  };

  res.status(result.status);
  res.render(viewFilePath, function (err) {
    if (err) { return res.json(result, result.status); }

    res.render(viewFilePath);
  });
};


module.exports[401] = function notAuthorized(req, res) {
  var viewFilePath = '401';
  var statusCode = 401;
  var result = {
    status: statusCode
  };
  logger.log('inside ' +statusCode)
  res.status(result.status);
  res.render(viewFilePath, function (err) {
    if (err) { return res.json(result, result.status); }

    res.render(viewFilePath);
  });
};
