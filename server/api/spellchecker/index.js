/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-11 12:21:38 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-03-18 14:47:00
 */


'use strict';

var express = require('express');
var spellCheckUtil = require('../../components/util/spellchecker')
var errors = require('../../components/errors')
var router = express.Router();
var auth = require('../../components/auth.service');
 

router.post('/',auth.isAuthenticated(),function(req,res){
    var userSpellingAutoCorrect = req.body.spellingCheckWord;
    console.log("----+++----")
    spellCheckUtil.userSpellingCorrection(userSpellingAutoCorrect, function (isError, returnedData) {
        if (!isError) {
            res.send({
                status: "Success",
                data: returnedData
            });
        }
        else {
            console.log("Error");
            res.send({
                status: "Failed",
                error: "Error ocurred while correcting user words correction"
            });
        }
    });
}).get(errors[401]);

module.exports = router;