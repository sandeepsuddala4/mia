/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-04 10:25:11 
 * @Last Modified by:   Sundeep Suddala 
 * @Last Modified time: 2019-03-04 10:25:11 
 */


'use strict';


var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var associatesLogsStoreSchema = new Schema({
    question: String,
    associateName: String,
    associateId: Number,
    associateCoversationTime: Date,
    action: String
 });

//Export
module.exports = mongoose.model('associatesLogsStoreSchema', associatesLogsStoreSchema);