/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-04 10:17:37 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-03-18 14:12:04
 */

'use strict';

var logSchema = require('./logs.model');

/**
 * 
 * @param {Request} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.save = function (req, res, next) {
    var logs = new logSchema(req);
    logs.associateCoversationTime = new Date();
    logs.save(function(err){
        if (!err && res) {
            res.send({
                status: "Success",
                data: 'Success'
            });
        }
        else {
            if(res)
            res.send({
                status: "Failed",
                error: "Error ocurred while creating user reaction"
            });
        }
    })
};


exports.get = function (req, res) {
    var logs = new logSchema(req);
    logs.get(function (data, isError) {
        if (!isError) {
             res.send({
                 status: "Sucess",
                 data: data
             });
        } else {
            res.send({
                status: "Failed",
                error: "An unexpected error occurred while fetching event getAssociatesLogs"
            })
        }
    });
};
