/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-04 10:34:08 
 * @Last Modified by:   Sundeep Suddala 
 * @Last Modified time: 2019-03-04 10:34:08 
 */

'use strict';

var express = require('express');
var controller = require('./logs.controller');
var errors = require('../../components/errors')
var router = express.Router();
var auth = require('../../components/auth.service');

router.post('/save',auth.isAuthenticated(),controller.save).get(errors[401]);
//router.get('/get',auth.isAuthenticated(),controller.get).get(errors[401]);

module.exports = router;