/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-04 10:18:53 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-03-11 16:19:23
 */


'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var feedbackSchema = new Schema({
   question: String,
   reaction: String,
   associateId: Number,
   associateName: String,
   feedbackMessage: String,
   isEmail: Boolean,
   userQuestion: String,
   associateCoversationTime: Date
});

//Export
module.exports = mongoose.model('associatesReactionPost', feedbackSchema);

