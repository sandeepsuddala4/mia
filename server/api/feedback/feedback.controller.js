/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-04 10:18:41 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-03-11 16:26:17
 */

 
/**
 * Use standard naming convention for endpoints.
 */

'use strict';

var Feedback = require('./feedback.model');
var mailer = require('../../components/util/mailer');


exports.save = function (req, res, next) {
    var feedback = new Feedback(req.body);
    logger.log(feedback)
    if (feedback.reaction === 'liked') {
        feedback.associateCoversationTime = new Date();
        saveFeedBack(feedback,req,res);
    }
    else {
        if(feedback.isEmail){
            mailer.sendFeedBackEmail(feedback,req,res);
        }
        saveFeedBack(feedback,req,res);
    }
    
};


/**
 * 
 * @param {Feedback} feedback 
 */
function saveFeedBack(feedback,req,res){
    feedback.save(function(err){
        if (!err) {
            res.send({
                status: "Success",
                data: 'Success'
            });
        }
        else {
            res.send({
                status: "Failed",
                error: "Error ocurred while creating user reaction"
            });
        }
    })
}