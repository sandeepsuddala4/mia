
'use strict';

var express = require('express');
var controller = require('./feedback.controller');
var errors = require('../../components/errors')
var router = express.Router();
var auth = require('../../components/auth.service');

router.post('/save',auth.isAuthenticated(),controller.save).get(errors[401]);

module.exports = router;