/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-11 15:45:31 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-03-11 16:45:45
 */

'use strict';

var express = require('express');
var controller = require('./admin.controller');
var errors = require('../../components/errors')
var router = express.Router();

router.get('/dashboardanalytics', controller.dashboardanalytics);
router.delete('/deletealldashboardanalytics', controller.deleteassociateLogsDataStore);
router.get('/getassociatesfeedback', controller.getassociatesfeedback);

module.exports = router;