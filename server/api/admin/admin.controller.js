/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-11 15:46:18 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-03-11 16:11:28
 */


'use strict';

var _ = require('lodash');
var rest = require('../../components/rest');
var constants = require('../../components/constants');
var env  = require('../../config/environment');
var url = require('url'); // standard node module
var util = require('../../components/util');
var logs = require("../logs/logs.model");
var feedbacks = require("../feedback/feedback.model")

exports.dashboardanalytics = function(req, res) {
    logs.find({}, function (err, data) {
        if (!err && data) {
            res.send({
                status: "Sucess",
                data: data
            });
        } else {
            res.send({
                status: "Failed",
                error: "An unexpected error occurred while fetching event getAssociatesLogs"
            })
        }
     });
}

module.exports.deleteassociateLogsDataStore = function (req, res) {
    logs.remove({}, function (err, data) {
        if (err) {
            console.log('deleteassociateLogsDataStore err:' + err);
            return;
        } else {
            console.log('deleteassociateLogsDataStore success.');
            res.json(data);
        }
    });
};

module.exports.getassociatesfeedback = function(req, res){
    feedbacks.find({}, function (err, data) {
        if (!err && data) {
            res.send({
                status: "Sucess",
                data: data
            });
        } else {
            res.send({
                status: "Failed",
                error: "An unexpected error occurred while fetching event getAssociates Feedback"
            })
        }
     });
}

function handleError(res, err) {
  return res.status(err);
}


