/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-25 12:15:44 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-03-25 16:46:20
 */
'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var jwtUserSchema = new Schema({
   associateId:  { type : String , unique : true, required : true },
   token: { type : String , unique : true, required : true },
   createdOn: {type: Date , default: () => Date.now() },
   updatedOn: Date
});

//Export
module.exports = mongoose.model('jwtUser', jwtUserSchema);