'use strict';

var JWTUser = require('./jwt.model');

/**
 * get token by associateId of the user
 * @param {*} associateId 
 */
function getTokenByAssociateId(associateId){
    JWTUser.findOne({associateId: associateId+""}, function(err, doc) {
            if(err){
                return undefined;
            }
            else {
                return doc.token;
            }
    });
}


/**
 * save or update token of the user
 * @param {*} obj 
 */
function saveTokenOfAssociate(obj){
    var jwtUser = new JWTUser(obj);
    var query = {associateId: obj.associateId},
    update =  {  token: obj.token , updatedOn: new Date() },
    options = { upsert: true, new: true, setDefaultsOnInsert: true };

    // Find the document
    JWTUser.findOneAndUpdate(query, update, options, function(error, result) {
        if (error) return false;
        else {
            return true;
        }
    });
}


/**
 * Check if the token is valid or not
 * @param {*} obj 
 */
function isValidToken(obj, callback){
    JWTUser.find({associateId:obj.associateId+"", token: obj.token}, function(err, doc){
        if(err || doc.length <= 0){
            callback(false);
        }
        else {
            callback(true);
        }
    })
}

function newUser(){
    return new JWTUser();
}

module.exports =  {
    isValidToken:isValidToken,
    saveToken: saveTokenOfAssociate,
    getToken: getTokenByAssociateId,
    newUser : newUser
}