/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-04 10:18:33 
 * @Last Modified by:   Sundeep Suddala 
 * @Last Modified time: 2019-03-04 10:18:33 
 */

'use strict';

var express = require('express');
var controller = require('./auth.controller');
var errors = require('../../components/errors')
var router = express.Router();

router.get('/', controller.index).get(errors[401]);

module.exports = router;