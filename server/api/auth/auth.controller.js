

/*
 * @Author: Sundeep Suddala 
 * @Date: 2019-03-04 10:18:04 
 * @Last Modified by: Sundeep Suddala
 * @Last Modified time: 2019-03-11 11:59:04
 */


/**
 * Use standard naming convention for endpoints.
 */

'use strict';

var _ = require('lodash');
var rest = require('../../components/rest');
var constants = require('../../components/constants');
var env  = require('../../config/environment');
var url = require('url'); // standard node module
var util = require('../../components/util');

exports.index = function(req, res) {
  const loginName = req.query.loginName;
  var options = {};
  
  if( (env.requiredProcessEnv('NODE_ENV') == 'production' && req.headers.referer != undefined ) || env.requiredProcessEnv('NODE_ENV') === 'development' || env.requiredProcessEnv('NODE_ENV') === 'test'){
    logger.log('----------Controller---------');
    var referer = req.headers.referer;
    logger.log(referer);
    logger.log(util.isAllowedReferer(referer));
    if(util.isAllowedReferer(referer)){
      options.path = constants.ASSOCIATE_DETAILS.replace('{{0}}', loginName);
      options.sub = loginName;
      return rest.initChat(req, options, res);
    }
    else {
      res.sendStatus(401).send();    
    }
  }
  else if(env.requiredProcessEnv('NODE_ENV') == 'production'){
    handleError(res, 401)
  }
  //res.sendStatus(401).send();
}

function handleError(res, err) {
  return res.status(err);
}


