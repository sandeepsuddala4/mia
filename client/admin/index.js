'use strict';

var express = require('express');
var router = express.Router();


router.get('/', function(req, res){
    res.sendFile(__dirname+'/dashboard.html')
});

module.exports = router;