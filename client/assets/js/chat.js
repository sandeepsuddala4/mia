var me = {};
me.avatar = "male1.jpg";

var you = {};
you.avatar = "/assets/img/female1.png";
var userEnterQuestion;
var appURL = getAssociateDetails().url;

var url = $(location).attr('href');
var host = appURL;
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
//var associateName = getUrlParameter("associateName");
var associateID = getAssociateDetails().associateId;
var associateName = getAssociateDetails().associateName;
var associateNameWelcomeMessage = associateName.split(",", 1);
var associateIDWelcomeMessage = getAssociateDetails().associateId;


function who(msg) {
    if (msg.indexOf('Me:') === 0) {
        return "me";
    }
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}



//-- No use time. It is a javaScript effect.
function insertChat(who, text, smallTalk, defaultIntent) {
    var control = "";
        var htmlInputOn = '';
        var date = formatAMPM(new Date());
        var removedMEString = text.replace(/^Me:+/i, '');
        var removedHRString = text.replace(/^HR:+/i, '');
        removedHRString = urlify(removedHRString);
        var acronym;
        var matches = JSON.stringify(associateName).match(/\b(\w)/g);
        acronym = matches.join('');
        if (who == "me") {
            userEnterQuestion = removedMEString;
            control = '<li style="width:100%">' +
                '<p data-letters=' + acronym + ' style="margin-bottom: -1rem;"></p>' +
                '<div class="msj macro" style="">' +
                '<div class="text text-l">' +
                '<p>' + removedMEString + '</p>' +
                '<p style="margin-bottom: 0rem;margin-left: 3rem;"><small>' + date + '</small></p>' +
                '</div>' +
                '</div>' +
                '</li>';
            $("#typingIndicator").show();
        } else {
            var n = $("li").length;
            var questionsTotalLength;
            var explode = function () {
                if (!Array.isArray(removedHRString) && new RegExp(/\?,/g).test(removedHRString)) {
                    var latestStringData = removedHRString.split('?,');
                    questionsTotalLength = latestStringData.length;
                    htmlInputOn += '<span>Am still learning,did you mean one of the following? Please click on questions</span>';
                    htmlInputOn += '<ol id="questionsList" style="list-style-type: circle;color: blue;">';
                    for (var i = 0; i < latestStringData.length; i++) {
                        var characterValue = latestStringData[i].indexOf("?");
                        var questionText;
                        if (characterValue == -1) {
                            questionText = latestStringData[i] + "?"
                        } else {
                            questionText = latestStringData[i];
                        }
                        htmlInputOn += '<li>' + '<a id="Textlink' + i + '"  style="font-size: 12px;height: auto;word-break: break-word; cursor:pointer;"  >' + questionText + '</a>' + '</li>';
                    }
                    // htmlInputOn += '<span style="font-size: 11px;" id="loadMore">Show More</span>';
                    // htmlInputOn += '<span style="font-size: 11px;" id="showLess">Show Less</span>';
                    htmlInputOn = htmlInputOn + '</ol>';
                } else {
                    var bgColor = ''
                    if(removedHRString){
                        try{
                            var jsonBody = JSON.parse(removedHRString.trim());
                            var messageType = jsonBody.messageType ;
                            if( messageType == 'gtlNominees' ) {
                                htmlInputOn = presentGTLNomineeDetails(jsonBody);
                            }
                            else  if( messageType == 'mediclaimNominees' ) {
                                htmlInputOn = presentMediclaimNomineeDetails(jsonBody);
                            }
                            else if ( messageType == 'fprRatings'){
                                htmlInputOn = presentLastFPRDetails(jsonBody);
                            }
                            else if ( messageType == 'glbDetails'){
                                htmlInputOn = presentGLBDetails(jsonBody);
                            }
                            else if ( messageType == 'leaveBalance'){
                                htmlInputOn = presentLeaveTypeMessage(jsonBody);
                            }
                            else if ( messageType == 'personalDetails'){
                                htmlInputOn = presentPersonalDetails(jsonBody);
                            }
                            else if(messageType == 'FBE'){
                                htmlInputOn = presentFBEDetails(jsonBody);
                            }
                            else if(messageType == 'birthday'){
                                htmlInputOn = jsonBody.message;
                                bgColor = 'background: #2d1b55 !important;';
                            }
                            else if(messageType == 'basicSalary'){
                                htmlInputOn = presentBasicSalary(jsonBody)
                            }
                            else if(messageType == 'incomeTax'){
                                htmlInputOn = presentIncomeTaxDeduction(jsonBody)
                            }
                            else if(messageType == 'deductions'){
                                htmlInputOn = presentDeductions(jsonBody)
                            }
                            else if(messageType == 'ptdDeductions'){
                                htmlInputOn = presentPTDDeductions(jsonBody)
                            }
                        }catch( e ){
                            console.log(e);
                            htmlInputOn = removedHRString;
                        }
                        if(htmlInputOn == '') {
                            htmlInputOn = JSON.parse(removedHRString);
                        }
                    }
                }
                $("#typingIndicator").hide();
                control = '<li style="width:100%;margin-left: 22px;padding-top: 5px">' +
                '<div class="" style="float:left;padding:0px 0px 0px 0px !important;">' + '<img class="img-circle" style="margin-top:6px;width:30px;height:30px;" src="' + host+you.avatar + '" />' + '</div>' +   
                '<div class="msj-rta macro" style="float:left; '+bgColor+'">' +
                    '<div class="text text-r" >' +
                    '<p class="comments-space">' + htmlInputOn + '</p>' +
                    '<p style="margin-bottom: 0rem;margin-left: 3rem;"><a class="like" title="response helpful"><i class="fa fa-thumbs-o-up" style="margin-right: 21px;"></i></a><a class="unlike" title="response not helpful"><i class="fa fa-thumbs-o-down" style="margin-right: 20px;"></i></a><small>' + date + '</small></p>' +
                    '</div>' +
                    '</div>' +
                    
                    '</li>';
                    bgColor = '';
                showMoreLink();
                var audio = new Audio('/assets/sounds/snap.ogg');
                audio.play();
                var listIdArrayLike = [];
                var listIdArrayUnLike = [];
                $("ul#messages").append(control);
                $(".like").each(function (n) {
                    $(this).attr("id", "like" + n);
                    listIdArrayLike.push($(this).attr("id"));
                });
                $(".unlike").each(function (n) {
                    $(this).attr("id", "unlike" + n);
                    listIdArrayUnLike.push($(this).attr("id"));
                });
                $('.like').on('click', function () {
                    if(!$(this).hasClass('liked')){
                        $(this).addClass('liked');
                        onLikeClicked(userEnterQuestion, removedHRString, $(this).attr("id"));
                    }
                });
                $('.unlike').on('click', function () {
                    if(!$(this).hasClass('unliked')){
                        $(this).addClass('unliked');
                        onUnLikeClicked(userEnterQuestion, removedHRString, $(this).attr("id"));
                    }
                });
                $("ul").each(function (n) {
                    var lastIndexLike = "#" + listIdArrayLike[listIdArrayLike.length - 1];
                    var lastIndexUnLike = "#" + listIdArrayUnLike[listIdArrayUnLike.length - 1];
                    if (smallTalk === 'smalltalk' || defaultIntent === 'Default Fallback Intent') {
                        $(lastIndexLike).hide();
                        $(lastIndexUnLike).hide();
                    } else {
                        $(lastIndexLike).show();
                        $(lastIndexUnLike).show();
                    }
                });
                // var size_li = $("#questionsList li").length;
                // questionsLoader(questionsTotalLength, size_li);
            };
            setTimeout(explode, 1000);
        }
        $("ul#messages").append(control);

        //var iframe = $(document).find('#chatFrame');
        $(document).find('.mainDiv').animate({scrollTop: $(document).find('.mainDiv')[0].scrollHeight},1000);
        //window.scrollTo(0, document.body.scrollHeight);
    //* End *//
}

function questionsLoader(questionSLength, size_li) {
    $(document).ready(function () {
        //* Questions Show Less and More *//
        x = 5;
        $('#questionsList li:lt(' + x + ')').show();
        $('#showLess').hide();
        $('#loadMore').click(function () {
            x = (x + 5 <= size_li) ? x + 5 : size_li;
            $('#questionsList li:lt(' + x + ')').show();
            if (questionSLength == size_li) {
                $('#loadMore').hide();
                $('#showLess').show();
            }
        });
        $('#showLess').click(function () {
            x = (x - 5 < 0) ? 3 : x - 5;
            if (x !== 0) {
                $('#questionsList li').not(':lt(' + x + ')').hide();
            }
            $('#showLess').hide();
            $('#loadMore').show();
        });
    });
}


function onLikeClicked(userEnterQuestion, removedHRString, aTagID) {
    var anchorID = "#" + aTagID + '.unlike';
    var atagID = "#" + aTagID + ' .fa-thumbs-o-up';
    var atagDown = "#" + "unlike" + aTagID.charAt(aTagID.length - 1) + ' .fa-thumbs-o-down';
    // $(atagDown).off('click');
    $(atagID).css("color", "#15A4CD");
    $(atagDown).css("color", "");
    $.ajax({
        url: appURL + "/api/feedback/save",
        type: 'POST',
        data: JSON.stringify({
            userQuestion: userEnterQuestion,
            question: removedHRString,
            reaction: "liked",
            associateId: associateIDWelcomeMessage,
            associateName: associateNameWelcomeMessage
        }),
        headers: {
            "Content-type": 'application/json',
            "Authorization": 'Bearer '+getUrlParameter('token')
        },
        dataType: 'json',
        success: function (data) {
            if(data.status == 'Success')
            $("#likedSuccess").fadeTo(2000, 500).slideUp(500, function () {
                $("#likedSuccess").slideUp(500);
            });
        }
    });
    // $('.alert-success').show();
    
}

function onUnLikeClicked(userEnterQuestion, removedHRString, aTagID) {
    $('#myModalSpan').click();
    $('#success-sent').hide();
    var anchorID = "#" + aTagID + '.like';
    var atagID = "#" + aTagID + ' .fa-thumbs-o-down';
    var atagDown = "#" + "like" + aTagID.charAt(aTagID.length - 1) + ' .fa-thumbs-o-up';
    // $(atagDown).off('click');
    $(atagID).css("color", "#ED2B28");
    $(atagDown).css("color", "");
    $('#message-text').val('');
    $('#submitFeedback').click(function (e) {
        var isEmail = $('#feedbackEmail').prop('checked');
        var feedbackMessage = $('#message-text').val();
        $('#message-text').css('border-color', '');
        if (feedbackMessage) {
            $.ajax({
                url: appURL + "/api/feedback/save",
                type: 'POST',
                data: JSON.stringify({
                    userQuestion: userEnterQuestion,
                    question: removedHRString,
                    reaction: "unliked",
                    associateId: associateIDWelcomeMessage,
                    associateName: associateNameWelcomeMessage,
                    feedbackMessage: feedbackMessage,
                    isEmail: isEmail
                }),
                headers: {
                    "Content-type": 'application/json',
                    "Authorization": 'Bearer '+getUrlParameter('token')
                },
                dataType: 'json',
                success: function (data) {
                    $('#success-sent').show();
                    $("#success-sent").fadeTo(2000, 500).slideUp(500, function () {
                        $("#success-sent").slideUp(500);
                    });
                    $('#feedbackBtnClose').click();
                }
            });
        } else {
            $('#message-text').css('border-color', 'red');
        }
    })
}


function resetChat() {
    $("ul#messages").empty();
}


function urlify(text) {
    var urlRegex = /(((https?:\/\/)|(www\.))[^\s]+)/g;
    return text.replace(urlRegex, function (url, b, c) {
        var url2 = (c == 'www.') ? 'http://' + url : url;
        return '<a href="' + url2 + '" target="_blank">click here</a>';
    })
}

function showMoreLink() {
    /* Show more ans less */
    $(document).ready(function () {
        var showChar = 150;
        var ellipsestext = "...";
        var moretext = "See More";
        var lesstext = "See Less";
        $('.comments-space').each(function () {
            var content = $(this).text();
            if (content.length > showChar) {
                var show_content = content.substr(0, showChar);
                var hide_content = content.substr(showChar, content.length - showChar);
                var html = show_content + '<span class="moreelipses">' + ellipsestext + '</span><span class="remaining-content"><span>' + hide_content + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
                $(this).html(html);
            }
        });
        $(".morelink").click(function () {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
    });
    /* End */
}

//-- Clear Chat
resetChat();

//-- Print Messages
// insertChat("me", "Hello Tom...", 0);
// insertChat("you", 'Just to let u know, chat conversation will be recorded.', '', 'Default Fallback Intent');

// setTimeout(function(){ $(".alert-info").css('display','none'); }, 5000);


function presentGTLNomineeDetails(jsonBody){
    var gtlNominees = jsonBody['gtlNominees'];
    var messageList = '';
    if(jsonBody.gtlNominees != undefined && Object.keys(jsonBody.gtlNominees).length > 0){
        jsonBody.gtlNominees.forEach(function (element) {
            var detailedInfo = '';
            if(element != undefined){
                Object.keys(element).forEach(function(key){
                    if(key != 'dependentName' && key != 'percentage'){
                        var li = '<li>'+key.replace(/([A-Z]+)/g, " $1").replace(/([A-Z][a-z])/g, " $1")+' : <a href="#">'+element[key]+' </a> </li>';
                        detailedInfo+=li;
                    }
                });
            }
            var list = '<li class="clickables"><a href="#" >'+element.dependentName+'( '+element.percentage+'% )</a><ul>'+detailedInfo+'</ul></li>';
            messageList+=list;
            });
        var detailsArray = '<ul>'+messageList+'</ul>';
        var random = Math.random().toString(36).substring(7);
        var message = '<div class="gtldetails" id="'+random+'"><p>  As per the information shared by you, your dependents  for GTL on iKARE are '+detailsArray+'</p></div>';
        minimizeLists(random);
    }
    else {
         message = '<div class="gtldetails" <p>  As per the information shared by you, there are no dependents  for GTL on iKARE</p></div>';    
        }
    return message;
}


// 
function presentMediclaimNomineeDetails(jsonBody){
    var gtlNominees = jsonBody['mediclaimNominees'];
    var messageList = '';
    var message = '';
    if(jsonBody.dependents != undefined && jsonBody.dependents.length >0 ){
        jsonBody.dependents.forEach( function (element) {
            var detailedInfo = '';
            if(element != undefined){
                Object.keys(element).forEach(function(key){
                    if(key != 'dependentName'){
                        var li = '<li>'+key.replace(/([A-Z]+)/g, " $1").replace(/([A-Z][a-z])/g, " $1")
                        +' : <a href="#">'+element[key]+' </a> </li>';
                        detailedInfo+=li;
                    }
                });
            }
            var list = '<li class="clickables"><a href="#" >'+element.dependentName+'</a><ul>'+detailedInfo+'</ul></li>';
            messageList+=list;
        });
        var detailsArray = '<ul>'+messageList+'</ul>';
        var random = Math.random().toString(36).substring(7);
        message = '<div class="gtldetails" id="'+random+'"><p>  As per the information shared by you, your dependents  for medical claims on iKARE are '+detailsArray+'</p></div>';
        minimizeLists(random);
    }
    else {
        message = '<div class="gtldetails" id="'+random+'"><p>  As per the information shared by you, there are records no found regarding medical claims</div>';
    }
    
    return message;
}

function presentLastFPRDetails(jsonBody){
    var gtlNominees = jsonBody['appraisalList'];
    var messageList = '';
    var cycleName = '';
    var rating = '';
    var message = '';
    var random = Math.random().toString(36).substring(7);
    if(jsonBody.appraisalList != undefined && jsonBody.appraisalList.length != 0){
        cycleName =  jsonBody.appraisalList[0].cycleName;
        rating = jsonBody.appraisalList[0].rating;
        message = '<div id="'+random+'">Your last FPR rating as per records is <span class="colored">'+rating +'</span> for the cycle  <span class="colored">'+ cycleName+ '</span></div>';
        if(parseFloat(rating) > 3.5) {
           // message+= '<div> That\'s a  great performance. You proved, pleasure in the job puts perfection in the work. Keep it up &#x263A</div>';
           // message ='<div class="comments-space">'+message+'</div>'
        }
    }
    else {
        message+= '<div > Sorry!! I didn\'t find any record of your performance. </div>'
    }    
    return message;
}


function presentGLBDetails(jsonBody){
    var messageList = '';
    var message = '';
    if(jsonBody.glbList != undefined && jsonBody.glbList.length >0 ){
        jsonBody.glbList.forEach(function(element) {
            var detailedInfo = '';
            if(element != undefined){
                Object.keys(element).forEach(function(key){
                    if(key != 'glbType'){
                        var li = '<li>'+key.replace(/([A-Z]+)/g, " $1").replace(/([A-Z][a-z])/g, " $1")
                        +' : <a href="#">'+element[key]+' </a> </li>';
                        detailedInfo+=li;
                    }
                });
            }
            var list = '<li class="clickables"><a href="#" >'+element.glbType+'</a><ul>'+detailedInfo+'</ul></li>';
            messageList+=list;
        });
        var detailsArray = '<ul>'+messageList+'</ul>';
        var random = Math.random().toString(36).substring(7);
        message = '<div class="gtldetails" id="'+random+'"><p>  Please specify which GLB are you referring'+detailsArray+'</p></div>';
        minimizeLists(random);
    }
    else {
        message = '<div class="gtldetails" id="'+random+'"><p>  Sorry there is no information regarding your GLB details</div>';
    }

    return message;
}


function presentLeaveTypeMessage(jsonBody){
    var message ='';
    var leaveType = jsonBody['leaveType'];
    if(leaveType != undefined){
        message = 'Your current '+jsonBody.leaveBalances.leaveName+'  balance is <span class="colored">'+jsonBody['leaveBalances']['totalBalance']+'</span>'
    }
    else {
        message +='Your current leave balance is as follows:'
        var list= '';
        jsonBody.leaveBalances.forEach(function(element) {
            var li = '<li>'+element.leaveName+' : <span class="colored"> '+ element.totalBalance + '</span> </li>';
            list+=li;
        });
        message+= '<div class="gtldetails"><ul>'+list+'</ul></div>';
    }
    return message;
}

function presentPersonalDetails(jsonBody){
    var message ='';
    var detailType = jsonBody['type'];
    try {
        if(detailType != undefined  && detailType != 'ALL') {
            message = 'Your '+detailType+' is <span class="colored">'+jsonBody['data']['value']+'</span>'
        }
        else if(detailType != undefined && detailType == 'ALL'){
            message +='Your Personal details are as follows:'
            var list= '';
            jsonBody.data.forEach(function(element) {
                var li = '<li>'+element.id+' : <span class="colored"> '+ element.value + '</span> </li>';
                list+=li;
            });
            message+= '<div class="gtldetails"><ul>'+list+'</ul></div>';
        }
        else {
            message+= '<div class="gtldetails">Sorry no details found</div>';
        }
    }
    catch(e){
        message+= '<div class="gtldetails">Sorry no details found</div>';
    }
    return message;
}


function presentFBEDetails(jsonBody){
    var message ='';
    try {
        message +='';
        var list= '';
        if(jsonBody.data != undefined && jsonBody.data.length > 0){
            var detailedInfo ='';
            
            const formatter = new Intl.NumberFormat('en-IN', {
                style: 'currency',
                currency: 'INR',
                minimumFractionDigits: 2
            });
            var element = jsonBody.data[0];
                if(element != undefined){
                    if(element['type'] == 'remaining'){
                        message = '<div class="gtldetails" id="'+random+'"><p>  Your balance amount of Flexible Benifits is <a>'+formatter.format(element.balanceAmount)+'</a></p></div>';
                    } 
                    else if(element['type'] == 'claimed'){
                        message = '<div class="gtldetails" id="'+random+'"><p>  Your have claimed <a>'+formatter.format(element.claimedAmount)+' from Flexible Benifits </a></p></div>';
                    }
                    else{
                        message = '<div class="gtldetails" id="'+random+'"><p>  Your eligiblity for Flexible Benifits is <a>'+formatter.format(element.eligibleAmount)+'</a></p></div>';
                    }
                }           
        }
        else {
            message = '<div class="gtldetails">Sorry no details found</div>';
        }
        }
        catch(e){
            message = '<div class="gtldetails">Sorry no details found</div>';
        }
        
    return message;
}

function presentDeductions(jsonBody){
    var message ='';
    try {
        const formatter = new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2
          });
        
        message +='';
        var list= '';
        jsonBody.deductions.forEach(function(element) {
           var detailedInfo ='';
            if(element != undefined){
                if(jsonBody.timePeriod === 'Month') {
                    var li = '<li>'+element.labelName
                    +' : <a >'+formatter.format(element.payAmount.amountValue)+' </a> </li>';
                    detailedInfo+=li;
                }
                else {
                    var li = '<li>'+element.labelName
                    +' : <a >'+formatter.format(element.payAmount.amountValue)+' </a> </li>';
                    detailedInfo+=li;
                }
                message+=detailedInfo;
            }
        });
        var detailsArray = '<ul>'+message+'</ul>';
        var random = Math.random().toString(36).substring(7);
        
        message = '<div class="gtldetails" id="'+random+'"><p>  Your deductions are as follows '+detailsArray+'</p></div>';
        
       // message+= '<div class="gtldetails"><ul>'+list+'</ul></div>';
        
    }
    catch(e){
        message = '<div class="gtldetails">Sorry no details found</div>';
    }
    return message;
}


function presentBasicSalary(jsonBody){
    var message ='';
    try {
        const formatter = new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2
          });
        var formatted = formatter.format(jsonBody.basicSalary.payYTDAmount.amountValue);
        if(typeof formatted != NaN)
            message+='<div class="gtldetails">Your basic Salary is <a>'+formatter.format(jsonBody.basicSalary.payYTDAmount.amountValue)+'</a> </div>';
        else {
            throw new Error("Error while parsing");
        }
    }
    catch(e){
        message = '<div class="gtldetails">Sorry no details found</div>';
    }
    return message;
}

function presentIncomeTaxDeduction(jsonBody){
    var message ='';
    try {
        const formatter = new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2
          });
          var formatted = formatter.format(jsonBody.incomeTax.payYTDAmount.amountValue);
          if(typeof formatted != NaN)
              message+='<div class="gtldetails">Your Income tax deduction is <a>'+formatted+'</a> </div>';
          else {
              throw new Error("Error while parsing");
          }
    }
    catch(e){
        message = '<div class="gtldetails">Sorry no details found</div>';
    }
    return message;
}


function presentPTDDeductions(jsonBody) {
    var message ='';
    try {
        const formatter = new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2
          });
          var formatted = formatter.format(jsonBody.body.deductionPTDAmount);
          var message = 'Your contributed '+jsonBody.deductionType+' amount is <a>'+formatted+'</a>';
          if(typeof formatted != NaN)
              message ='<div class="gtldetails">'+message+'</div>';
          else {
              throw new Error("Error while parsing");
          }
    }
    catch(e){
        message = '<div class="gtldetails">Sorry no details found</div>';
    }
    return message;
}

function onQuestionButtonClick(question) {
    $("#typingIndicator").show();
    $('#m').val(question);
    $('form').submit();
    $("#typingIndicator").hide();
    window.scrollTo(0,document.body.scrollHeight);
}

function minimizeLists(random){
    setTimeout(function(){
        $(document).find('#'+random+'.gtldetails ul > li > ul').hide();
        $(document).find('#'+random+'.gtldetails ul > li > ul').css('pointer-events','none'); 

        $(document).find('#'+random+'.gtldetails ul li:has(ul)').click(function(e){
            if($(this).children('ul').length>0){
                e.preventDefault();
                if($(this).children('ul').is(':visible')){
                    $(this).find('ul').slideUp(),
                    $(this).removeClass("expanded");
                }else{
                    $(this).find('ul').slideDown(),
                    $(this).addClass("expanded"); 
                }
            }
        });
    }, 50);
}

$(document).ready(function(){
    $(document).on('click', 'li a[id^="Textlink"]', function(){
        $("#typingIndicator").show();
        $('#m').val($(this).text());
        $('form').submit();
        $("#typingIndicator").hide();
        window.scrollTo(0,document.body.scrollHeight);
    });
    var time = new Date(); console.log(time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds());
    console.log(time);
    //insertWelcomeMessage();

    $(document).on('click', '#menuDiv .menu_trigger', function(){
        $('.optionsDiv').slideToggle();
        if($(this).hasClass('expanded')){
            $(this).removeClass('expanded').addClass('collapsed');
            $(this).find('.fa-chevron-up').addClass('fa-chevron-down').removeClass('fa-chevron-up')
        }
        else if($(this).hasClass('collapsed')){
            $(this).removeClass('collapsed').addClass('expanded');
            $(this).find('.fa-chevron-down').addClass('fa-chevron-up').removeClass('fa-chevron-down');
        }

    });
    
});


function insertWelcomeMessage(){
    var isBirthDay = false, isWeddingDay = false;
    var dateOfBirth = getAssociateDetails().dateOfBirth;
    var weddingDate = getAssociateDetails().weddingDate;
    if(dateOfBirth != undefined && dateOfBirth != 'undefined' && dateOfBirth != '' && weddingDate != 't'){
        isBirthDay = true;
    }

    if(weddingDate != undefined && weddingDate != 'undefined' && weddingDate != '' && weddingDate != 't'){
        isWeddingDay = true;
    }

    if(isWeddingDay && isBirthDay ){
        var body = {};
        body.messageType = "birthday";
        var html = '<div class="birthdayDiv"> <div class="wrapper"> <div id="scene"> <div class="snow"> <div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div></div><div class="text"> <h1> Happy Birthday and Anniversary </h1> <h2 id="hbd_associate_name">'+associateNameWelcomeMessage+'</h2> </div><div class="tree one"></div><div class="tree two"></div><div class="tree three"></div><div class="tree four"></div><div class="tree five"></div></div><div class="platform"> <div class="quote">Live long, Stay Happy - MIA</div></div></div></div>'
        body.message = html;
        insertChat("you", JSON.stringify(body), '', 'Default Fallback Intent');
    }
    else if(isBirthDay){
        //
        var body = {};
        body.messageType = "birthday";
        var html = '<div class="birthdayDiv"> <div class="wrapper"> <div id="scene"> <div class="snow"> <div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div></div><div class="text"> <h1> Happy Birthday </h1> <h2 id="hbd_associate_name">'+associateNameWelcomeMessage+'</h2> </div><div class="tree one"></div><div class="tree two"></div><div class="tree three"></div><div class="tree four"></div><div class="tree five"></div></div><div class="platform"> <div class="quote">Live long, Stay Happy - MIA</div></div></div></div>'
        body.message = html;
        insertChat("you", JSON.stringify(body), '', 'Default Fallback Intent');
        //insertChat("you", 'Hi ' + associateNameWelcomeMessage + ', how can i help you today?', '', 'Default Fallback Intent');
    }
    else if(isWeddingDay){
        var body = {};
        body.messageType = "birthday";
        var html = '<div class="birthdayDiv"> <div class="wrapper"> <div id="scene"> <div class="snow"> <div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div><div class="flake"></div></div><div class="text"> <h1> Happy Anniversary </h1> <h2 id="hbd_associate_name">'+associateNameWelcomeMessage+'</h2> </div><div class="tree one"></div><div class="tree two"></div><div class="tree three"></div><div class="tree four"></div><div class="tree five"></div></div><div class="platform"> <div class="quote">Live long, Stay Happy - MIA</div></div></div></div>'
        body.message = html;
        insertChat("you", JSON.stringify(body), '', 'Default Fallback Intent');
    }
    else {
        insertChat("you", 'Hi ' + associateNameWelcomeMessage + ', how can i help you today?', '', 'Default Fallback Intent');       
    }
    setTimeout(function(){
        optionMessage(questionCategories);
    },1000);
}



function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toDateString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
  
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
        c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
        }
    }
    return "";
}


function optionMessage(questionCategories){
    var html = '<div class="optionsDiv"><div class="msg-action"><div style="margin-bottom: 12px;font-weight: 600;/* font-family: &quot;Calibri&quot;; */color: #3774b7;">Get started by asking</div>';
    questionCategories.forEach(function(category){
        html+= '<span onclick="javascript:listQuestionsOfCat('+"'"+category.categoryName+"'"+')" class="action-elem"><span >'+category.categoryName+'</span></span>';
    });
    html+='</div></div>';
    $('#menuDiv .optionsDiv').remove();
    $('#menuDiv').append(html);
}


function listQuestionsOfCat(categoryName){
    $('#menuDiv .menu_trigger').click();
    insertChat("me", categoryName, '', 'Default Fallback Intent');
    $("#typingIndicator").show();
    var cat = $.grep( questionCategories, function( n, i ) {
        if(n.categoryName=== categoryName) {
            return questionCategories[i];
        }
      });
    if(cat.length >0){
        cat = cat[0];
    }
    var htmlInputOn = '<div>Please select one of the following </div><ol id="questionsList" style="list-style-type: circle;color: blue;">';
      for (var i = 0; i < cat.questions.length; i++) {
          var questionText = cat.questions[i];
          htmlInputOn += '<li>' + '<a id="Textlink' + i + '"  style="font-size: 12px;height: auto;word-break: break-word; cursor:pointer;"  >' + questionText + '</a>' + '</li>';
      }
      // htmlInputOn += '<span style="font-size: 11px;" id="loadMore">Show More</span>';
      // htmlInputOn += '<span style="font-size: 11px;" id="showLess">Show Less</span>';
      htmlInputOn = htmlInputOn + '</ol>';
      insertChat("you", htmlInputOn, '', 'Default Fallback Intent');

}



var questionCategories = [
    {
        "categoryName":"Salary Details",
        "questions":[
            "What is my basic salary?",
            "What is my income tax deduction this year?",
            "What is the total amount deducted from my salary in last month?",
        ]
    },
    {
        categoryName:"Leave Balances",
        questions:[
            "What is my current leave balance for this fiscal year?",
            "What is my current PL balance for this fiscal year?",
            "What is my current CL balance for this fiscal year?",
            "What is my current OH balance for this fiscal year?",
            "How many leaves have I carried forward from last year?"
        ]
    },
    {
        categoryName: "Heirarchy",
        questions: [
            "Who is my skip level manager?",
            "Who is my group manager?",
            "Show my hierarchy"
        ]
    },{
        categoryName: "YCC",
        questions: [
            "What is the total amount that I have contributed so far towards YCC?",
            "What is the total amount that I have contributed so far towards NPS?"
        ]
    },
    {
        categoryName: "Flexible Benifits",
        questions: [ 
            "What is my eligibility for Flexible benefit?",
            "What is the flexible amount remaining in my account to be claimed for?"
        ]
    },
    {
        categoryName: "Nominees",
        questions: [
            "Who all are included as my dependents for GTL?",
            "Who all are included as my dependents for medi-claim?"
        ]
    },{
        categoryName: "Personal Details",
        questions: [
            "What is my UAN number?",
            "What is my PAN number?"
        ]
    }
];
