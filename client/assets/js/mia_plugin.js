var miaHost ="http://localhost:3000";

function createMIA(token){
    
    var html = '<a class="button" id="mialauncher" tooltip="MIA" ><img src="'+miaHost+'/assets/img/female1.png"></a>';
    html+= '<div class="chatbox"><div class="header"><h5 class="title">MIA</h5>';
    html+= '<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    html+= '<iframe id="chatFrame" ></iframe></div>'
    html+= '<style> .hidden {display: none;}';
    html+='.chatbox {border-radius: 5px; height: 720px;max-height: 500px;display: none; min-height: 0; width: 380px; opacity: 1;bottom: 15px;overflow: hidden; position: fixed; right: 15px; z-index: 2147483600;box-shadow: 1px 3px 5px 2px rgba(0,0,0,.26);}'
    html+='#mialauncher {box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15); border-radius: 50%;  display: block;width: 56px; height: 56px; margin: 20px auto 0; position: relative; -webkit-transition: all .1s ease-out; transition: all .1s ease-out;position: absolute; right: 30px; bottom: 30px; cursor: pointer; box-shadow: 1px 3px 5px 2px rgba(0,0,0,.26); }';
    html+= '#mialauncher img {max-width: 100%;}';
    html+= '#mialauncher .fa {font-size: 4rem;}';
    html += 'body {height: 100vh;min-height: 100vh; }';
    html += '.chatbox iframe{width: 100% !important; height: calc(90%) !important;border: 0px !important; border-radius: 5px;margin-top: -3px; }';
    html += '.chatbox .header  {background-color: #27374e !important; border-radius: 5px 5px 0 0; box-shadow: 0 0 6px rgba(0,0,0,.3); display: inline-block; height: auto; top: 0; transition: .4s all; width: 100%; z-index: 100; color: white !important; padding: 12px 20px;}';
    html += '.chatbox .header h5 { width: fit-content; float: left; margin-bottom: 0px;}';
    html += '.chatbox button.close {color: white; opacity: 1 !important; cursor: pointer;}';
    html += '.mainDiv { padding-top: 65px; } #sendmessage button.input-group-addon { border-radius: 0px .25rem .25rem 0px; margin: 0;margin-left: 0px; border: 0;}';
    html += '</style>';
    $('body').append(html);
    $(document).find("#chatFrame").prop('src',miaHost+"/app/?token="+token);
}

function encrypt(){
    var key = btoa(btoa(JSON.stringify(window.mia)));
    return key;
}

 

function addMIA(loginName){
    $(document).ready(function(){
        $.ajax({
        url: miaHost+"/api/auth?loginName="+loginName,
        type: 'GET',
        headers: {
          "Content-type": 'application/json'
        },
        dataType: 'json',
        success: function (data) {
          var token = data.token;  
          createMIA(token);
          addMIA = function(){
              alert("Not allowed to do it");
          }
        }
      });
    $(document).on('click','#mialauncher', function(){
        $(document).find('.chatbox,#mialauncher').toggle();
        var iFrameDOM = document.getElementById('chatFrame').contentWindow;
        iFrameDOM.insertWelcomeMessage();
    });
        
    $(document).on('click','.chatbox .close', function(){
        $(document).find('.chatbox,#mialauncher').toggle();
        var iFrameDOM = document.getElementById('chatFrame').contentWindow;
        iFrameDOM.resetChat();
    });
  });
}