/* global io */
'use strict';

var socket = io.connect(host, {
  query: {
    associateId: associateID
  }
});