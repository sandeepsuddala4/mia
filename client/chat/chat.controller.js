'use strict';

var express = require('express');
var router = express.Router();
var auth = require('../../server/components/auth.service');
var config = require('../../server/config/environment');
const jwt =  require('jsonwebtoken');
const encryptor = require('../../server/components/util/crypto');

module.exports.index = function(req,res){
    var token = req.query.token;
    var decoded = jwt.verify(token, config.requiredProcessEnv('JWT_SECRET'));
    console.log('----------------------------chat-------------------------');
    var userDetails = encryptor.decryptUserDetails(decoded.apd);
    decoded = userDetails;
     if(decoded == undefined || decoded.associateName == undefined) {
         res.render('500.ejs');
     }
     else if(config.requiredProcessEnv('NODE_ENV') == 'production'){
        if(decoded.token == undefined){
            res.render('401.ejs');
        }
        else if(decoded.origin !== req.headers.origin){
            res.render('401.ejs');
        }
     }
     else {
        var body = {};
        body.associateName = decoded.associateName;
        body.dateOfBirth = decoded.apdd;
        body.weddingDate = decoded.apdw;
        body.associateId = decoded.associateId;
        body.url = decoded.url;
        res.render('ejs/chat.ejs', body);
     }
    
}

