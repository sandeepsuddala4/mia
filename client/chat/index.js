'use strict';

var express = require('express');
var router = express.Router();
var auth = require('../../server/components/auth.service');
var config = require('../../server/config/environment');
const jwt =  require('jsonwebtoken');
const controller = require('./chat.controller');


router.get('/',  auth.isAuthenticated(), controller.index);

module.exports = router;